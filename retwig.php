<?php

    function scan_dir($dirname) 
    { 
        
        // метки к которым следует добавить фильтр raw
        $raw = ["mainmenu",
                "entity.announce",
                "navigation",
                "entity.main_text",
                "comments",
                "comments_form",
                "www_root",
                "forum.last_post",
                "forum.who_is_here",
                "theme.last_user",
                "theme.last_page",
                "theme.themeicon",
                "user.edit_profile_link",
                "post.message",
                "post.editor_info",
                "reply_form",
                "comment.avatar",
                "comment.name_a",
                "comment.message",
                "new_user",
                "context.cats_selector",
                "editor_head",
                "editor_body",
                "editor_buttons",
                "editor_text",
                "editor_forum_text",
                "editor_forum_quote",
                "editor_forum_name",
                "entity.attachment",
                "result.announce",
                "context.rules",
                "context.bday_selector",
                "context.bmonth_selector",
                "context.byears_selector",
                "data.message",
                "message.text",
                "message.delete"];

        // метки к которым следует добавить префикс atm.
        $atm = ["mainmenu",
                "navigation",
                "comments",
                "comments_form",
                "www_root",
                "template_path",
                "title",
                "meta_description",
                "meta_keywords",
                "all_online",
                "users_online",
                "pagination",
                "categories",
                "config",
                "getOrderLink",
                "getAvatar",
                "checkAccess",
                "add_link",
                "entities",
                "__",
                "atm_user",
                "checkUserOnline",
                "getUserRatingImg",
                "getUserRatingText",
                "forum_cats",
                "context",
                "forum.subforums",
                "forum.count_themes_here",
                "themes",
                "fps_admin_access",
                "guests_online",
                "posts",
                "reply_form",
                "count_themes",
                "count_posts",
                "count_users",
                "new_user",
                "max_online_all_time",
                "max_online_all_time_date",
                "today_born_users",
                "fps_request_url",
                "editor_head",
                "editor_body",
                "editor_buttons",
                "editor_text",
                "editor_forum_text",
                "editor_forum_quote",
                "editor_forum_name",
                "getProfileUrl",
                "modules",
                "data.message",
                "data.url",
                "data.jsredirect",
                "messages"];

        $atm_u = ["user.name",
                "user.id",
                "user.edit_profile_link",
                "user.stat",
                "user.rating",
                "user.avatar",
                "user.regdate",
                "user.lastvisit",
                "user.rank_img",
                "user.full_name",
                "user.baned",
                "user.group_color",
                "user.group",
                "user.posts",
                "user.warnings",
                "user.age",
                "user.byear",
                "user.bmonth",
                "user.bday",
                "user.url",
                "user.pol",
                "user.add_field_1",
                "user.lastpost",
                "user.about",
                "user.signature"];

        // какие файлы являются дочерними к main.html.twig
        $block = ["list",
                "material",
                "addform",
                "editform",
                "catlist",
                "themes_list",
                "showuserinfo",
                "edituserform",
                "edituserformbyadmin",
                "posts_list",
                "editpostform",
                "forum_passwd_form",
                "search_list",
                "addnewuserform",
                "viewrules",
                "pm",
                "pm_view"];

        $replace = ["atm_page_num" => "page_num",
                "atm_pages_cnt" => "pages_cnt",
                "atm_page_prev" => "page_prev",
                "atm_page_next" => "page_next",
                "atm_server_name" => "server_name",
                "fps_request_url" => "request_url",
                "fps_wday" => "wday",
                "fps_wday_n" => "wday_",
                "fps_date" => "date",
                "fps_time" => "time",
                "fps_hour" => "hour",
                "fps_minute" => "minute",
                "fps_day" => "day",
                "fps_month" => "month",
                "fps_year" => "year",
                "atm_user" => "user",
                "fps_users_edit" => "users_edit",
                "fps_admin_access" => "admin_access",
                "fps_rss" => "rss",
                "fps_users_groups" => "users_groups"];

        $replace_search = [
                "atm.modules" => "atm.form.modules",
                "atm.context" => "atm.form.context",
                'method="POST"' => 'method="GET"',
                'name="search"' => 'name="q"',
                '<input  type="submit" value="Искать" name="send" />' => '<button type="submit">Искать</button>'];

        // Открываем текущую директорию 
        $dir = opendir($dirname); 
        // Читаем в цикле директорию 
        while (($file = readdir($dir)) !== false) 
        {
            // Если файл обрабатываем его содержимое 
            if($file != "." && $file != "..") 
            { 
                // Если имеем дело с файлом - производим в нём замену 
                if(is_file($dirname."/".$file)) 
                { 
                    // название текущего файла
                    $file_exp = explode(".", $file);
                    // название текущей папки
                    $simple_dir = array_reverse( explode('/', $dirname) );
                    $simple_dir = $simple_dir[0];

                    // Читаем содержимое файла 
                    $content = file_get_contents($dirname."/".$file); 
                    // Осуществляем замену


                    $arr1 = ["{% include '../",
                                "{% include \"../"];
                    $arr2 = ["{% include '",
                                "{% include \""];
                    $content = str_replace($arr1, $arr2, $content);

                    $arr1 = [".html' %}",
                                ".html\" %}"];
                    $arr2 = [".html.twig' %}",
                                ".html.twig\" %}"];
                    $content = str_replace($arr1, $arr2, $content);

                    foreach ($raw as $item) {
                        $content = str_replace("".$item." }}", "".$item." | raw }}", $content);
                    }

                    foreach ($atm as $item) {
                        $content = str_replace("% ".$item, "% atm.".$item, $content);
                        $content = str_replace("{ ".$item, "{ atm.".$item, $content);
                        $content = str_replace("$ ".$item, "$ atm.".$item, $content);
                        $content = str_replace("f ".$item, "f atm.".$item, $content);
                        $content = str_replace("n ".$item, "n atm.".$item, $content);
                        $content = str_replace("d ".$item, "d atm.".$item, $content);
                        $content = str_replace("(".$item, "(atm.".$item, $content);
                    }

                    if (mb_substr($file, 0, 9) == 'material.') {
                        $content = str_replace(" entity", " atm.entity", $content);
                        $content = str_replace("(entity", "(atm.entity", $content);
                    }

                    $content = str_replace("checkAccess([module,", "checkAccess([atm.module,", $content);
                    $content = str_replace("{{{", "{ {{", $content);
                    $content = str_replace("AtmDate(user.byear ~ '-' ~ user.bmonth ~ '-' ~ user.bday, 'j F Y')", "(user.byear ~ '-' ~ user.bmonth ~ '-' ~ user.bday) | adate('j F Y')", $content);

                    $content = preg_replace("/mb_strtolower\((\S+)\)/", "$1 | lower", $content);
                    $content = preg_replace("/str_replace\(( *)(\S+)( *),( *)(\S+)( *),( *)(\S+)( *)\)/", '\8 | replace({\2: \5})', $content);
                    $content = preg_replace("/AtmDate\((\S+)\)/", "$1 | adate", $content);
                    $content = preg_replace("/getOrderLink\((\S+)\) }/", "getOrderLink($1) | raw }", $content);

                    // main.html
                    $content = str_replace("{{ content }}", "{% block content %}{% endblock %}", $content);
                    $content = str_replace(
                        "/news/add_form/\" class=\"lastpostsbutton button\">Добавить новость</a>",
                        "/{{ atm.module }}/add_form/\" class=\"lastpostsbutton button\">Добавить материал</a>", $content);


                    if ($file_exp[0] == 'showuserinfo') {
                        foreach ($atm_u as $item) {
                            $content = str_replace(" ".$item, " atm.".$item, $content);
                            $content = str_replace("(".$item, "(atm.".$item, $content);
                        }
                    }

                    if ($file_exp[0] == 'moder_panel') {
                        $content = str_replace(" module ", " atm.module ", $content);

                        if (strpos($content, "{% set entity = atm.entity %}") === false) {
                            $content = "{% if atm.entity %}\n{% set entity = atm.entity %}\n{% endif %}".$content;
                        }
                    }
                    
                    // дочеризация файлов
                    if (in_array($file_exp[0], $block) && strpos($content, "{% extends \"main.html.twig\" %}") === false && $simple_dir != 'chat') {
                        $content = "{% extends \"main.html.twig\" %}\n\n{% block content %}\n\n".$content."\n{% endblock %}";
                    }
                    
                    
                    
                    // chat
                    $content = str_replace("{{ fps_chat }}", "{% include 'chat/list.html.twig' %}\n
                        {% include 'chat/addform.html.twig' %}", $content);
                    $content = str_replace("{% if fps_chat", "{% if atm.chat", $content);
                    if ($file_exp[0] == 'addform' && $simple_dir == "chat") {
                        $content = str_replace("{{ data.message }}", "", $content);
                        $content = str_replace("{{ data.", "{{ atm.chat.", $content);
                    }
                    
                    
                    
                    // подключение get_stat.html.twig на форуме
                    if ($file_exp[0] == 'catlist' && strpos($content, "get_stat.html.twig") === false) {
                        $content = str_replace("{% endblock %}", "{% include 'forum/get_stat.html.twig' %}\n{% endblock %}", $content);
                        $content = str_replace("{{ data.", "{{ atm.chat.", $content);
                    }

                    
                    
                    // подключение viewcomment.html.twig
                    if ($file_exp[0] == 'material' && strpos($content, "viewcomment.html.twig") === false) {
                        $content = str_replace("{% endblock %}", "{% include 'default/viewcomment.html.twig' %}\n{% endblock %}", $content);
                    }
                    if ($file_exp[0] == 'viewcomment') {
                        $content = str_replace("entities", "comments", $content);
                    }
                    // подключение addcommentform.html.twig
                    if ($file_exp[0] == 'material' && strpos($content, "addcommentform.html.twig") === false) {
                        $content = str_replace("{% endblock %}", "{% include 'default/addcommentform.html.twig' %}\n{% endblock %}", $content);
                    }
                    if ($file_exp[0] == 'addcommentform') {
                        $content = str_replace("context", "comments_form", $content);
                    }
                    $content = str_replace("{{ atm.comments | raw }}", "", $content);
                    $content = str_replace("{{ atm.comments_form | raw }}", "", $content);
                    
                    $content = str_replace("{{ atm.editor_forum_name ", "{{ atm.editor_forum_name( post.author.name ) ", $content);
                    $content = str_replace("{{ atm.editor_forum_quote ", "{{ atm.editor_forum_quote( post.author.name ) ", $content);
                    
                    if ($file_exp[0] == 'posts_list') {
                        $content = str_replace(" theme.", " atm.theme.", $content);
                    }
                    
                    // импорт формы поисковой страницы
                    if ($simple_dir == 'search' && strpos($content, "search_form.html.twig") === false) {
                        $content = str_replace("{{ context.form }}", "{% include 'search/search_form.html.twig' %}", $content);
                        $content = str_replace("{{ atm.context.form }}", "{% include 'search/search_form.html.twig' %}", $content);
                    }
                    // обновление на новые метки поисковой формы
                    if ($simple_dir == 'search' && $file_exp[0] == 'search_form') {
                        foreach ($replace_search as $key => $item) {
                            $content = str_replace($key, $item, $content);
                        }
                    }
                    
                    // обновление на новые метки
                    foreach ($replace as $key => $item) {
                        $content = str_replace($key, $item, $content);
                    }
                    
                    $content = str_replace("{{ atm_css }}", '', $content);
                    $content = str_replace("{{ atm_js }}", "{% include 'default/atm_css_js.html.twig' %}", $content);

                    // Перезаписываем файл 
                    file_put_contents($dirname."/".$file,$content);
                    
                    if (mb_substr($file, -4) == 'html') {
                        rename($dirname."/".$file, $dirname."/".$file.".twig");
                    }
                } 
                // Если перед нами директория, вызываем рекурсивно 
                // функцию scan_dir 
                if(is_dir($dirname."/".$file)) 
                { 
                    print ($dirname."/".$file."<br>"); 
                    scan_dir($dirname."/".$file); 
                } 
            } 
        } 
        // Закрываем директорию 
        closedir($dir); 
    }

    if (isset($_GET['t'])) {
        $root = realpath(__DIR__);
        $dirname = $root."/template/".$_GET['t']."/html";         
        print($dirname.'<br>');
        if(is_dir($dirname)) {
            scan_dir($dirname);  // Вызов рекурсивной функции
        } else {
            print('not fount');
        }
        if(!is_file($dirname.'/main.html.twig')) {
            copy($dirname.'/default/main.html.twig', $dirname.'/main.html.twig');
        }
        if(!is_file($dirname.'/users/comments.html.twig')) {
            file_put_contents($dirname.'/users/comments.html.twig', "{% extends \"main.html.twig\" %}\n{% block content %}\n{% include 'default/viewcomment.html.twig' %}\n{% endblock %}");
        }
        if(!is_file($dirname.'/default/atm_css_js.html.twig')) {
            file_put_contents($dirname.'/default/atm_css_js.html.twig', "{% for css in atm.css %}\n<link rel=\"stylesheet\" href=\"{{ css.src }}\"{% if css.async %} async{% endif %}{% if css.defer %} defer{% endif %} />\n{% endfor %}\n\n{% for js in atm.js %}\n<script type=\"text/javascript\" src=\"{{ js.src }}\"{% if js.async %} async{% endif %}{% if js.defer %} defer{% endif %} ></script>\n{% endfor %}");
        }
        if(!is_dir($dirname."/pages")) {
            mkdir($dirname."/pages");
        }
        if(!is_file($dirname.'/pages/default.html.twig')) {
            file_put_contents($dirname.'/pages/default.html.twig', "{% extends \"main.html.twig\" %}\n\n{% block content %}\n{{ atm.content | raw }}\n{% endblock %}");
        }
    } else {
        print ('example: <b>site.com/retwig.php?t=your_template</b>');
    }
  
?>

