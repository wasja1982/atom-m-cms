<?php
/**
* @project    Atom-M CMS
* @package    Stat Sections Model
* @url        https://atom-m.net
*/


namespace StatModule\ORM;

class StatCategoriesModel extends \OrmModel {
    public $Table = 'stat_categories';
}