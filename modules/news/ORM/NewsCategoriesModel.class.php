<?php
/**
* @project    Atom-M CMS
* @package    News Sections Model
* @url        https://atom-m.net
*/


namespace NewsModule\ORM;

class NewsCategoriesModel extends \OrmModel {
    public $Table = 'news_categories';
}