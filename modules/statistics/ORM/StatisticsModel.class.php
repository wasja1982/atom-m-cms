<?php
/**
* @project    Atom-M CMS
* @package    Statistics Model
* @url        https://atom-m.net
*/


namespace StatisticsModule\ORM;

class StatisticsModel extends \OrmModel
{
    public $Table = 'statistics';

    protected $RelatedEntities;

}