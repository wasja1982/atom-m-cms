<?php
/**
* @project    Atom-M CMS
* @package    Loads Sections Model
* @url        https://atom-m.net
*/


namespace LoadsModule\ORM;

class LoadsCategoriesModel extends \OrmModel {
    public $Table = 'loads_categories';
}