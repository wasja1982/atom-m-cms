<?php

namespace ForumModule;

class EventsHandler {
    static $support_events = array(
        "after_parse_global_markers",
        "new_user"
    );

    function after_parse_global_markers($markers) {
        
        $markers['checkAccessForum'] = function($params) {
            return \ForumModule\ActionsHandler::checkAccessForum($params);
        };
        
        return $markers;
    }
    
    function new_user($params) {
        $Register = \Register::getInstance();
        $actions = isset($Register['params']) ? $Register['params'] : explode('/', trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), '/'));
        if ($actions && is_array($actions) && count($actions) && $actions[0]) {
            $module = $actions[0];
            $Cache = new \Cache('pages');
            if ($Cache->check($module . '_get_stat')) {
                $Cache->remove($module . '_get_stat');
            }
        }
    }
}