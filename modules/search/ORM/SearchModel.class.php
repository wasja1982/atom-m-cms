<?php
/**
* @project    Atom-M CMS
* @package    News Model
* @url        https://atom-m.net
*/


namespace SearchModule\ORM;

class SearchModel extends \OrmModel
{
    public $Table = 'search_index';

    protected $RelatedEntities = array();


    public function createIndex() {
        // Truncate index
        getDB()->query("TRUNCATE `" . getDB()->getFullTableName('search_index') . "`");
        
        $modules = array();
        $installed_modules = \ModuleManager::getInstalledModules();
        if (!empty($installed_modules) && is_array($installed_modules)) {
            foreach($installed_modules as $module) {
                $model_name = \OrmManager::getModelName($module);
                if (method_exists($model_name, '__getQueriesForSearch')) {
                    $modules[] = $module;
                }
            }
        }
        if (count($modules)) {
            // Generate list of indexed additional fields
            $add_fields = getDB()->select('add_fields', DB_ALL, array('cond' => array('indexed' => '1', '`module` IN (\'' . implode("','", array_merge($modules, array('comments'))) . '\')')));
            $indexed_fields = array();
            if (is_array($add_fields) && count($add_fields)) {
                foreach ($add_fields as $field) {
                    $indexed_fields[$field['module']][] = 'add_field_' . $field['field_id'];
                }
            }

            $queries = array();
            
            // Queries for comments if necessary
            $comments_modules = array();
            foreach($modules as $module) {
                $module_name = '\\' . ucfirst($module) . 'Module\ActionsHandler';
                if (method_exists(ucfirst($module_name), '_add_comment_form')) {
                    $comments_modules[] = $module;
                }
            }
            if (count($comments_modules)) {
                $queries[] = "SELECT `message` as `index`, `id` as `entity_id`, 'comments' as `entity_table`, CONCAT('/view/', `entity_id`, '#comment') as `entity_view`, `module`, GREATEST(`date`,`editdate`) as `date` FROM `" .  getDB()->getFullTableName('comments') . "` WHERE `message` IS NOT NULL AND `message` <> '' AND `module` IN ('" . implode("','", $comments_modules) . "')";
                if (isset($indexed_fields['comments'])) {
                    foreach ($indexed_fields['comments'] as $field) {
                        $queries[] = "SELECT `$field` as `index`, `id` as `entity_id`, 'comments' as `entity_table`, CONCAT('/view/', `entity_id`, '#comment') as `entity_view`, `module`, GREATEST(`date`,`editdate`) as `date` FROM `" .  getDB()->getFullTableName('comments') . "` WHERE `$field` IS NOT NULL AND `$field` <> '' AND `module` IN ('" . implode("','", $comments_modules) . "')";
                    }
                }
            }
            
            foreach ($modules as $module) {
                $model = \OrmManager::getModelInstance($module);
                $module_queries = @$model->__getQueriesForSearch();
                if (is_array($module_queries) && count($module_queries)) {
                    foreach ($module_queries as $q) {
                        $queries[] = $q;
                    }
                }
                if (isset($indexed_fields[$module])) {
                    foreach ($indexed_fields[$module] as $field) {
                        $queries[] = "SELECT `$field` as `index`, `id` as `entity_id`, '$module' as `entity_table`, '/view/' as `entity_view`, '$module' as `module`, `date` FROM `" .  getDB()->getFullTableName($module) . "` WHERE `$field` IS NOT NULL AND `$field` <> ''";
                    }
                }
            }
            $query = '';
            foreach ($queries as $q) {
                $query .= (!empty($query) ? ' UNION ' : '') . $q;
            }
            getDB()->query("INSERT INTO `" .  getDB()->getFullTableName('search_index') . "` (`index`, `entity_id`, `entity_table`, `entity_view`, `module`, `date`) " . $query);
        }
    }

    public function getSearchResults($search, $limit, $modules)
    {
        $lmsql = '';
        if (is_array($modules)) {
            $lmsql .= '(';
            foreach ($modules as $module) {
                if ($module != $modules[0]) {
                    $lmsql .= ' OR ';
                }
                $lmsql .= '`module` = \''.$module.'\'';
            }
            $lmsql .= ') AND';
        }
        $results = getDB()->query("
            SELECT * FROM `" . getDB()->getFullTableName('search_index') . "`
            WHERE ".$lmsql." MATCH (`index`) AGAINST ('" . $search . "' IN BOOLEAN MODE)
            ORDER BY MATCH (`index`) AGAINST ('" . $search . "' IN BOOLEAN MODE) DESC LIMIT " . $limit);
        if ($results) {
            $results_modules = array();
            foreach ($results as $key => $res) {
                $results_modules[$res['module']][$key] = new SearchEntity($res);
            }

            foreach ($results_modules as $module => $results_module) {
                $model_name = \OrmManager::getModelName($module);
                if (\ModuleManager::checkInstallModule($module) && \Config::read('active', $module) && method_exists($model_name, '__correctSearchResults')) {
                    $model = \OrmManager::getModelInstance($module);
                    $results_modules[$module] = $model->__correctSearchResults($results_module);
                }
            }
            
            foreach ($results_modules as $module => $results_module) {
                foreach ($results_module as $key => $result) {
                    $results[$key] = $result;
                }
            }
        }

        return $results;
    }
}