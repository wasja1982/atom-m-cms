<?php

$code = http_response_code();

if (is_numeric($code)) {
    $headers = array(
        '404' => "HTTP/1.0 404 Not Found",
        '403' => "HTTP/1.0 403 Forbidden You don't have permission to access / on this server.",
        '429' => "HTTP/1.1 429 Too Many Requests",
    );
    if (!empty($headers[$code])) header($headers[$code]);
}
include_once dirname(__FILE__).'/../boot.php';

switch ($code) {
    case '429':
        if (\Config::read('request_per_second', '__secure__')) {
            header('Retry-After: ' . \Config::read('request_per_second', '__secure__'));
        }
        $html = @file_get_contents(ROOT . '/data/errors/hack.html.twig');
        break;
    case '403':
        $html = @file_get_contents(ROOT . '/data/errors/' . (isset($reason) ? ($reason == 'ban' ? 'ban.html.twig' : 'closed.html.twig') : '403.html.twig'));
        break;
    case '404':
        $html = @file_get_contents(ROOT . '/data/errors/404.html.twig');
        break;
    default:
        $html = @file_get_contents(ROOT . '/data/errors/default.html.twig');
}

$Viewer = new Viewer_Manager();

$markers = array();
$markers['code'] = $code;
$markers['site_title'] = \Config::read('site_title');
$markers['site_domain'] = $_SERVER['SERVER_NAME'];
if ($code == '403' && isset($reason) && $reason == 'closed') {
    if (\UserAuth::isGuest()) {
        $markers['login'] = get_url('users/login');
    } elseif (\UserAuth::isUser()) {
        $markers['name'] = \UserAuth::getField('name');
        $markers['logout'] = get_url('users/logout');
    }
    if (\Config::read('closed_message')) {
        $markers['message'] = \Config::read('closed_message');
    }
    if (\Config::read('autorization_protected_key', '__secure__') === 1) {
        $_SESSION['form_key_mine'] = rand(1000, 9999);
        $form_key = rand(1000, 9999);
        $_SESSION['form_hash'] = md5($form_key . $_SESSION['form_key_mine']);
        $markers['form_key'] = '<input type="hidden" name="form_key" value="' . $form_key . '" />';
    }
}

echo $Viewer->parseTemplate($html, array('error' => $markers));

?>
