<?php
/**
* @project    Atom-M CMS
* @package    Events Class
* @url        https://atom-m.net
*/


class Events {
    
    static $map = null;
    static $classes = null;
    
    static function create_map() {
        self::$map = array();
        self::$classes = array();
        $installed_modules = \ModuleManager::getInstalledModules();
        if (!empty($installed_modules) && is_array($installed_modules)) {
            foreach ($installed_modules as $priority => $module) {
                $class_name = ucfirst($module) . 'Module\EventsHandler';
                if (\Config::read('active', $module) && class_exists($class_name) && property_exists($class_name, 'support_events') && is_array($class_name::$support_events)) {
                    $support_events = $class_name::$support_events;
                    foreach ($support_events as $index => $value) {
                        $event = is_array($value) ? $index : $value;
                        $options = is_array($value) ? $value : array();
                        if (!empty($event)) {
                            $pr = isset($options['priority']) && is_numeric($options['priority']) ? intval($options['priority']) : $priority + 100;
                            self::$map[$event][$pr][] = $class_name;
                        }
                    }
                }
            }
        }
        $plugins = \PluginManager::getInstalledPlugins();
        if (!empty($plugins) && count($plugins) > 0) {
            foreach ($plugins as $priority => $plugin) {
                if (file_exists(\PluginManager::getConfigFile($plugin))) {
                    $config = json_decode(file_get_contents(\PluginManager::getConfigFile($plugin)), true);
                    $plugin_config = array();
                    if (is_array($config)) {
                        if (!isset($config['active']) || !$config['active'] || !isset($config['points']) || empty($config['points'])) {
                            continue;
                        }
                        if (is_string($config['points'])) {
                            $config['points'] = array($config['points']);
                        }
                        foreach ($config['points'] as $index => $value) {
                            $event = (is_numeric($value) && is_string($index)) ? $index : $value;
                            if (isset($config['action'])) {
                                $plugin_config['action'] = $config['action'];
                            }
                            $plugin_config['className'] = isset($config['className']) && !empty($config['className']) ? $config['className'] : $plugin;
                            if (!empty($event)) {
                                $pr = (is_numeric($value) && is_string($index)) ? intval($value) : $priority + 200;
                                self::$map[$event][$pr][$plugin] = $plugin_config;
                            }
                        }
                    }
                }
            }
        }
        // Sorting for priority
        foreach(self::$map as $event => $classes) {
            ksort($classes, SORT_NUMERIC);
            self::$map[$event] = $classes; 
        }
    }
    
    /**
    * Initialisation event
    *
    * @param string $event - name of event
    * @param mixed $params - variable parameters
    * @param mixed $etc - static parameters
    * @return mixed - changed $params
    */
    static function init($event, $params = null, $etc = null) {
        if (self::$map === null) {
            self::create_map();
        }
        // Calling modules
        if (!empty(self::$map) && isset(self::$map[$event]) && is_array(self::$map[$event])) {
            $Register = Register::getInstance();
            $actions = isset($Register['params']) ? $Register['params'] : explode('/', trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), '/'));
            foreach (self::$map[$event] as $priority => $class_names) {
                foreach ($class_names as $index => $value) {
                    if (is_string($index) && is_array($value)) { // Plugin
                        $plugin = $index;
                        $plugin_config = $value;
                        $class_name = isset($plugin_config['className']) ? $plugin_config['className'] : $plugin;
                        $skip = false;
                        // Checking event settings for plugin
                        if (!empty($plugin_config['action']) && $actions && is_array($actions)) {
                            $allow = isset($plugin_config['action']['allow']) && is_array($plugin_config['action']['allow']) ? $plugin_config['action']['allow'] : array();
                            $disallow = isset($plugin_config['action']['disallow']) && is_array($plugin_config['action']['disallow']) ? $plugin_config['action']['disallow'] : array();
                            foreach ($actions as $index => $action) {
                                // Skip if this event is disallow
                                if (!empty($action) && count($disallow) && isset($disallow[$index]) && in_array($action, $disallow[$index])) {
                                    $skip = true;
                                    break;
                                }
                            }
                            if (!$skip && count($allow)) {
                                $skip = true;
                                foreach ($actions as $index => $action) {
                                    // Don't skip if this event is allow
                                    if (!empty($action) && isset($allow[$index]) && in_array($action, $allow[$index])) {
                                        $skip = false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!$skip) {
                            // Create class if necessary
                            if (!isset(self::$classes[$class_name])) {
                                include_once \PluginManager::getIndexFile($plugin);
                                if (!class_exists($class_name)) {
                                    throw new Exception("Undefined classname '$class_name' in '$plugin' plugin.");
                                } else {
                                    self::$classes[$class_name] = new $class_name(null);
                                }
                                if (property_exists(self::$classes[$class_name], 'plugin_path')) {
                                    self::$classes[$class_name] = get_url('/plugins/' . $plugin);
                                }
                            }
                            $params = self::$classes[$class_name]->common($params, $event, $etc);
                            // TODO: Deprecated
                            // Processign tag {{ plugin_path }} (for old plugins)
                            if (is_string($params) && mb_strpos($params, 'plugin_path') !== false) {
                                $path = get_url('/plugins/' . $plugin);
                                $params = preg_replace('#{{\s*plugin_path\s*}}#i', $path, $params);
                            }
                        }
                    } else { // Module
                        $class_name = $value;
                        // Create class if necessary
                        self::$classes[$class_name] = isset(self::$classes[$class_name]) ? self::$classes[$class_name] : new $class_name();
                        // Call method if exists
                        if (is_object(self::$classes[$class_name]) && method_exists(self::$classes[$class_name], $event)) {
                            $params = self::$classes[$class_name]->{$event}($params, $etc);
                        }
                    }
                }
            }
        }
        return $params;
    }
}
