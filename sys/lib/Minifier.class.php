<?php
/**
* Uses minify CSS and JS
*
* @project    Atom-M CMS
* @package    Minifier class
* @url        https://atom-m.net
*/

class Minifier {
    
    /*
     * List of files
     */
    private $list = array();
    
    /*
     * Priority
     */
    private $priority = 100;
    
    /*
     * Type of files
     */
    private $css = true;


    /**
     * Loading need classes, create object of Minify and fill default list of files
     */
    public function __construct($css = true) {
        // Init classes of use
        $classes = array(
            'Converter.php',
            'Exception.php',
            'Minify.php',
            'CSS.php',
            'JS.php',
            'Exceptions' . DS . 'BasicException.php',
            'Exceptions' . DS . 'FileImportException.php',
            'Exceptions' . DS . 'IOException.php'
        );
        if (($merge_files = \Config::read('merge_files')) && isset($merge_files[$css ? 'css' : 'js']) && $merge_files[$css ? 'css' : 'js']) {
            // Loading need classes
            foreach ($classes as $class_file) {
                require_once realpath(__DIR__) . DS . 'Minify' . DS . $class_file;
            }
        }
        
        $param = $css ? '__css__' : '__js__';
        
        // Loading standart list for Atom-M
        $list = \Config::read($param);
        
        // Loading list for current template
        $template_path = '/template/' . getTemplate();
        if (file_exists(ROOT . $template_path . '/config/config.php')) {
            $template_config = include(ROOT . $template_path . '/config/config.php');
            if (isset($template_config[$param]) && is_array($template_config[$param])) {
                $list = array_merge($list, $template_config[$param]);
            }
        }
        
        // Correct markers
        foreach ($list as $index => $value) {
            $file = str_replace('__template_path__', $template_path, (is_array($value) ? $index : $value));
            $options = (is_array($value) ? $value : array());
            $this->add($file, $options, $template_path);
        }
        
        $this->css = $css;
    }
    
    
    /**
     * Add new file to list
     */
    public function add($file, $options = array(), $template_path = null) {
        $template_path = empty($template_path) ? '/template/' . getTemplate() : $template_path;
        $file = str_replace('__template_path__', $template_path, $file);
        $options = (!$options || !is_array($options) ? array() : $options);
        $Register = Register::getInstance();
        if (!empty($file) && (!isset($options['user']) || (isset($options['user']) && (
                ($options['user'] === false && isGuest()) ||
                ($options['user'] === true && isUser()) ||
                (is_numeric($options['user']) && $options['user'] == intval(\UserAuth::getField('status')))))) &&
                (!isset($options['module']) || (isset($options['module']) && isset($Register['module']) && mb_strtolower($options['module']) == $Register['module']))) {
            $options['priority'] = isset($options['priority']) && is_numeric($options['priority']) ? intval($options['priority']) : $this->priority++;
            $options['merge'] = isset($options['merge']) ? $options['merge'] : true;
            $this->list[$file] = $options;
        }
    }
    
    
    /**
     * Loading need classes
     * 
     * @return string
     */
    public function getHtml() {
        // Checking necessity to merge
        $merge_files = \Config::read('merge_files');
        $need_merge = is_array($merge_files) && isset($merge_files[$this->css ? 'css' : 'js']) && $merge_files[$this->css ? 'css' : 'js'];

        // Split list
        $merge_list = array();
        $no_merge_list = array();
        foreach ($this->list as $file => $options) {
            if ($need_merge && $options['merge']) {
                $merge_list[$options['priority']][] = $file; 
            } else {
                $no_merge_list[$options['priority']][$file] = $options; 
            }
        }
        ksort($merge_list, SORT_NUMERIC);
        
        if ($need_merge) {
            $use_gzip = \Config::read('merge_gzip');

            // Download remote files
            foreach ($merge_list as $priority => $files) {
                $dirname = '/sys/tmp/remote_' . ($this->css ? 'css' : 'js') . DS;
                foreach ($files as $index => $file) {
                    if (stripos($file, 'http://') !== false || stripos($file, 'https://') !== false) {
                        $filename = $dirname . md5($file) . ($this->css ? '.css' : '.js');
                        if (!file_exists(ROOT . $filename) || (file_exists(ROOT . $filename) && filemtime(ROOT . $filename) + 24 * 60 * 60 < time())) {
                            // Create directory if necessary
                            if (!file_exists(ROOT . $dirname)) {
                                mkdir(ROOT . $dirname, 0777);
                            }
                            // Download file
                            $data = @file_get_contents($file);
                            if ($data) {
                                @file_put_contents(ROOT . $filename, $data);
                            }
                        }
                        if (file_exists(ROOT . $filename)) {
                            $merge_list[$priority][$index] = $filename;
                        }
                    }
                }
            }
            
            // Check files edit time and generate name for cached file
            $min_priority = $this->priority;
            $maxtime = 0;
            $merge_hash = '';
            foreach ($merge_list as $priority => $files) {
                $min_priority = $min_priority >= $priority ? $priority : $min_priority;
                foreach ($files as $file) {
                    if ((stripos($file, 'http://') === false || stripos($file, 'https://') === false) && file_exists(ROOT . $file)) {
                        $filetime = filemtime(ROOT . $file);
                        $maxtime = $maxtime < $filetime ? $filetime : $maxtime;
                        $merge_hash .= $file;
                    } else {
                        // File not found
                        $no_merge_list[$priority][$file] = array();
                    }
                }
            }
            $filename = DS . 'data' . DS . 'cached' . DS . 'atom.min.' . md5($merge_hash) . '.' . ($this->css ? 'css' : 'js');
            
            // Merge if:
            // 1) merged file not found;
            // 2) merged file older than not merged files.
            if (!file_exists(ROOT . $filename) || filemtime(ROOT . $filename) < $maxtime || 
                ($use_gzip && (!file_exists(ROOT . $filename . '.gz') || filemtime(ROOT . $filename. '.gz') < $maxtime))) {
                // Create object
                $minifier = $this->css ? new Minify\CSS() : new Minify\JS();
                foreach ($merge_list as $priority => $files) {
                    foreach ($files as $file) {
                        if ((stripos($file, 'http://') === false || stripos($file, 'https://') === false) && file_exists(ROOT . $file)) {
                            $minifier->add(ROOT . $file);
                        } else {
                            // File not found
                        }
                    }
                }
                $minifier->minify(ROOT . $filename);
                touch(ROOT . $filename, $maxtime);
                if ($use_gzip) {
                    $minifier->gzip(ROOT . $filename . '.gz');
                    touch(ROOT . $filename . '.gz', $maxtime);
                }
            }
            if (file_exists(ROOT . $filename. ($use_gzip ? '.gz' : ''))) {
                $no_merge_list[$min_priority][$filename. ($use_gzip ? '.gz' : '')] = array();
            }
        }
        ksort($no_merge_list, SORT_NUMERIC);
        
        // Generate HTML
        $data_list = array();
        foreach ($no_merge_list as $files) {
            foreach ($files as $file => $options) {
                if (stripos($file, 'http://') === false && stripos($file, 'https://') === false) {
                    if (file_exists(ROOT . $file)) {
                        $file .= '?ver=' . (filemtime(ROOT . $file) % 31536000);
                    }
                    $file = get_url(str_replace(DS, '/', $file));
                }
                $data_list[] = array_merge($options, array('src' => $file));
            }
        }
        return $data_list;
    }
}
