<?php
/**
 * @project     Atom-M CMS
 * @package     Sitemap Generator
 * @url         https://atom-m.net
 */

if (function_exists('set_time_limit')) @set_time_limit(0);
if (function_exists('ignor_user_abort')) @ignor_user_abort();




class Sitemap {

    public $output;
    private $host;
    private $uniqUrl = array();
    private $DB;



    public function __construct($params = array()) {
        $this->host = $_SERVER['HTTP_HOST'] . '/';
        $this->uniqUrl[] = (used_https() ? 'https://' : 'http://') . $this->host;
        $this->DB = getDB();
    }


    /**
     * Создаёт карту сайта.
     * sitemap.xml должен быть разрешен для индексации поисковыми роботами.
     *
     */
    public function createMap() {
        $this->getLinks();
        $this->finalizeLinks();

        $this->output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n"
            . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"' . "\n"
            . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' . "\n"
            . 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' . "\n"
            . 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\n";

            foreach ($this->uniqUrl as $page) {
                if ((substr($page, 0, 7) !== 'http://') and (substr($page, 0, 8) !== 'https://')) $page = (used_https() ? 'https://' : 'http://') . AtmUrl::parseRoutes($page);
                $this->output .= '<url>' . "\n"
                    . '<loc>' . $page . '</loc>' . "\n"
                    . '<changefreq>daily</changefreq>' . "\n"
                    . '</url>' . "\n";
            }

            $this->output .= '</urlset>';

        file_put_contents(ROOT . '/sitemap.xml', $this->output);
    }



    /**
     * Удаление дубликатов и экранирование символов.
     */
    private function finalizeLinks() {
        $entities = array(
            '&' => '&amp;',
            '"' => '&quot;',
            '\'' => '&apos;',
            '<' => '&lt;',
            '>' => '&gt;',
        );

        $this->uniqUrl = array_unique($this->uniqUrl);
        foreach ($this->uniqUrl as $key => $link) {
            $link = trim($link, '/');
            $link = str_replace(array_keys($entities), $entities, $link);
            $this->uniqUrl[$key] = $link;
        }
    }



    /**
     * Построение ссылок на все материалы.
     */
    public function getLinks() {
        $installed_modules = \ModuleManager::getInstalledModules();
        if (!empty($installed_modules) && is_array($installed_modules)) {
            foreach($installed_modules as $module) {
                $model_name = \OrmManager::getModelName($module);
                if (\Config::read('active', $module) && method_exists($model_name, '__getSitemapList')) {
                    $model = \OrmManager::getModelInstance($module);
                    $list = $model->__getSitemapList($this->host);
                    $this->uniqUrl = array_merge($this->uniqUrl, $list);
                }
            }
        }
    }
}
?>