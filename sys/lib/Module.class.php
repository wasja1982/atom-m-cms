<?php
/**
* @project    Atom-M CMS
* @package    Module Class
* @url        https://atom-m.net
*/


class Module {

    /**
    * @page_title title of the page
    */
    public $page_title = '';
    /**
    * @var string
    */
    public $page_meta_keywords;
    /**
    * @var string
    */
    public $page_meta_description;
    /**
    * @template layout for module
    */
    public $template = 'default';
    /**
    * @categories list of categories
    */
    public $categories = '';
    /**
    * @module_title title for module
    */
    public $module_title = '';
    /**
    * @module current module
    */
    public $module = '';
    /**
    * @cacheTags Cache tags
    */
    public $cacheTags = array();
    /**
    * @cacheTags Cache key
    */
    public $cacheKey = '';
    /**
    * @cached   use the cache engine
    */
    protected $cached = true;

    /**
    * @var (str)   comments block
    */
    protected $comments = '';

    /**
    * @var (str)   add comments form
    */
    protected $comments_form = '';

    /**
    * @var    database object
    */
    protected $Database;
    /**
    * uses for work with actions log
    *
    * @var    logination object
    */
    protected $Log;
    /**
    * uses for work with parser (chuncks, snippets, global markers ...)
    *
    * @var    parser object
    */
    protected $Parser;
    /**
    * contains system settings
    *
    * @var (array)   system settings
    */
    public $set;

    /**
     * if true - counter not worck
     *
     * @var boolean
     */
    public $counter = true;

    /**
     * @var object
     */
    public $Register;

    /**
     * @var object
     */
    public $Model;



    /**
     * @var array
     */
    protected $globalMarkers = array(
        'module' => '',
        'navigation' => '',
        'pagination' => '',
        'meta' => '',
        'add_link' => '',
        'comments_pagination' => '',
        'comments' => '',
        'comments_form' => '',
        'atm_page_num' => 1,
        'atm_pages_cnt' => 1,
    );



    /**
     * @param array $params - array with modul, action and params
     *
     * Initialize needed objects adn set needed variables
     */
    function __construct($params)
    {
        $this->Register = Register::getInstance();
        $this->Register['module'] = $params[0];
        $this->Register['action'] = $params[1];
        $this->Register['params'] = $params;

        // Use for templater (layout)
        $this->template = $this->module;


        $this->View = new Viewer_Manager(array('layout' => $this->template));
        $this->DB = getDB();
        $this->isLogging = false;
        if (\Config::read('__secure__.system_log'))
            $this->isLogging = true;

        $this->beforeRender();

        $this->page_title = (\Config::read('title', $this->module)) ? h(\Config::read('title', $this->module)) : h($this->module);
        $this->params = $params;

        //cache
        $this->Cache = new \Cache('pages');
        if (\Config::read('cache') == 1) {
            $this->cached = true;
            $this->cacheKey = $this->getKeyForCache($params);
            $this->setCacheTag(array('module_' . $params[0]));
            if (!empty($params[1])) $this->setCacheTag(array('action_' . $params[1]));
        } else {
            $this->cached = false;
        }

        //meta tags
        $this->page_meta_keywords = h(\Config::read('keywords', $this->module));
        if (empty($this->page_meta_keywords)) $this->page_meta_keywords = h(\Config::read('meta_keywords'));
        $this->page_meta_description = h(\Config::read('description', $this->module));
        if (empty($this->page_meta_description)) $this->page_meta_description = h(\Config::read('meta_description'));
    }



    protected function setModel()
    {
        $this->Model = OrmManager::getModelInstance(ucfirst($this->module));
    }


    /**
     * Uses for before render
     * All code in this function will be worked before
     * begin render page and launch controller(module)
     *
     * @return none
     */
    protected function beforeRender()
    {
        if (isset($_SESSION['page'])) unset($_SESSION['page']);
        if (isset($_SESSION['pagecnt'])) unset($_SESSION['pagecnt']);
    }


    /**
     * Uses for after render
     * All code in this function will be worked after
     * render page.
     *
     * @return none
     */
    protected function afterRender()
    {
        // Cron
        if (\Config::read('auto_sitemap'))
            fpsCron('createSitemap', 86400);


        /*
        * counter ( if active )
        * and if we not in admin panel
        */
        if ($this->counter === false) return;
            
        \Events::init('user_pageviewed',$this);
    }


    /**
    * @param string $content  data for parse and view
    * @access   protected
    */
    protected function _view($content)
    {
        \Events::init('before_parse_layout', $this);

        if (\Config::read('debug_mode') == 1) {
            $render_time = round(getMicroTime() - $this->Register['fps_boot_start_time'], 4);
            \AtmDebug::addRow('Load Time', array('Booting and rendering duration', $render_time));
        }

        $this->afterRender();

        echo $content;

        if (\Config::read('debug_mode') == 1) {
            $render_time = round(getMicroTime() - $this->Register['fps_boot_start_time'], 4);
            \AtmDebug::addRow('Load Time', array('All duration', $render_time));
            echo \AtmDebug::getBody();
        }

    }


    protected function render($fileName, array $markers = array())
    {
        $additionalMarkers = $this->getGlobalMarkers();
        $this->_globalize($additionalMarkers);
        $source = $this->View->view($fileName, array_merge($markers, $this->globalMarkers));
        return $source;
    }

    
    protected function getGlobalMarkers($html = '')
    {
        $markers = array();
        $obj = $this;// Для анонимных функций

        $markers['module'] = $this->module;
        $markers['title'] = $this->page_title;
        $markers['meta_description'] = $this->page_meta_description;
        $markers['meta_keywords'] = $this->page_meta_keywords;
        $markers['module_title'] = $this->module_title;
        $markers['params'] = $this->params;
        $markers['categories'] = $this->categories;
        $markers['comments'] = $this->comments;
        $markers['comments_form'] = $this->comments_form;
        $markers['page_num'] = (!empty($this->Register['page'])) ? intval($this->Register['page']) : 1;
        $markers['pages_cnt'] = (!empty($this->Register['pagescnt'])) ? intval($this->Register['pagescnt']) : 1;
        $markers['page_prev'] = (!empty($this->Register['prev_page_link'])) ? $this->Register['prev_page_link'] : '';
        $markers['page_next'] = (!empty($this->Register['next_page_link'])) ? $this->Register['next_page_link'] : '';
        
        if (isset($this->params) && is_array($this->params)) {
            $markers['action'] = count($this->params) > 1 ? $this->params[1] : 'index';
            $markers['current_id'] = count($this->params) > 2 ? $this->params[2] : null;
        }

        $markers['used_https'] = used_https();
        $site_url = (used_https() ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $markers['site_url'] = array_merge( parse_url($site_url), array('full' => $site_url) );
        $markers['server_name'] = $_SERVER['SERVER_NAME'];
        $markers['request_url'] = $_SERVER['REQUEST_URI'];
        $markers['wday'] = date("D");
        $markers['wday_n'] = date("w");
        $markers['date'] = date("d-m-Y");
        $markers['time'] = date("H:i");
        $markers['hour'] = date("G");
        $markers['minute'] = date("i");
        $markers['day'] = date("j");
        $markers['month'] = date("n");
        $markers['year'] = date("Y");

        $path = \Config::read('smiles_set');
        $path = (!empty($path) ? $path : 'atomm');
        $markers['smiles_set'] = $path;

        $markers['smiles_list'] = function() use($path) {
            $path = ROOT . '/data/img/smiles/' . $path . '/info.php';
            include $path;
            if (isset($smilesList) && is_array($smilesList)) {
                return (isset($smilesInfo) && isset($smilesInfo['show_count'])) ? array_slice($smilesList, 0, $smilesInfo['show_count']) : $smilesList;
            } else {
                return array();
            }
        };

        $markers['powered_by'] = 'Atom-M CMS';
        $markers['site_title'] = \Config::read('site_title');

        $markers['template_path'] = get_url('/template/' . getTemplate());
        $markers['www_root'] = WWW_ROOT;
        $markers['lang'] = getLang();
        $markers['langs'] = (count(getPermittedLangs()) >= 1) ? getPermittedLangs() : '';

        $markers['mainmenu'] = function() use($obj) {

            $menu_conf_file = ROOT . '/sys/settings/menu.dat';
            if (!file_exists($menu_conf_file)) return false;
            $menudata = unserialize(file_get_contents($menu_conf_file));


            if (!empty($menudata) && count($menudata) > 0) {
                $out = $obj->buildMenuNode($menudata, 'class="fpsMainMenu"');
            } else {
                return false;
            }
            return $out;
        };

        // Generate CSS
        $markers['css'] = function() {
            return (new Minifier(true))->getHtml();
        };
        
        // Generate JS
        $markers['js'] = function() {
            return (new Minifier(false))->getHtml();
        };
        
        // Для получения некоторых значений регистра, так можно сэкономить на fetch и запросах к бд, если нужные данные уже получались на этойже странице.
        $markers['register'] = array();
        $markers['register']['current_vars'] = $obj->Register['current_vars'];
        $markers['register']['categories'] = $obj->Register['categories'];
        $markers['register']['shared'] = $obj->Register['shared'];


        /** Метки, значение которых вычисляется только если они были вызваны в шаблонизаторе */
        $atm_user = array();
        $markers['user'] = function() use(&$atm_user) {

            if (\UserAuth::isUser()) {
                $atm_user = \UserAuth::getUser();
                unset($atm_user['passw']);
                $atm_user['profile'] = getProfileUrl(\UserAuth::getField('id'));
                $atm_user['group_id'] = \UserAuth::getField('status');
                $atm_user['group'] = function() {
                    $userGroup = \ACL::getGroup(\UserAuth::getField('status'));
                    return $userGroup['title'];
                };

                $get_difference_time = (time() - strtotime(\UserAuth::getField('puttime'))) / 86400;
                if ($get_difference_time < 0) $get_difference_time = 0;

                $atm_user['reg_days'] = round($get_difference_time);

                $atm_user['avatar_url'] = getAvatar(\UserAuth::getField('id'));

                $atm_user['unread_pm'] = function() {
                    $Cache = new \Cache('users/new_pm', 'messages');
                    if ($Cache->check('user_' . \UserAuth::getField('id')))
                        $res = $Cache->read('user_' . \UserAuth::getField('id'));
                    else {
                        $usersModel = OrmManager::getModelInstance('Users');
                        $res = $usersModel->getNewPmMessages(\UserAuth::getField('id'));
                        $Cache->write($res, 'user_' . \UserAuth::getField('id'),array());
                    }
                    if ($res)
                        return $res;
                    else
                        return 0;
                };


            } else {
                $atm_user['profile'] = get_url('/users/add_form/');
                $atm_user['id'] = 0;
                $atm_user['name'] = __('Guest');
                $atm_user['group'] = __('Guests');
                $atm_user['reg_days'] = '';
                $atm_user['unread_pm'] = '';
                $atm_user['avatar_url'] = getAvatar();
            }

            $atm_user['admin_access'] = (\ACL::turnUser(array('__panel__', 'entry'))) ? '1' : '0';

            return $atm_user;
        };


        // today borned users
        $markers['today_born_users'] = function() {
            $today_born = getBornTodayUsers();
            $tbout = '';
            if (count($today_born) > 0) {
                $names = array();
                foreach ($today_born as $user) {
                    $names[] = get_link($user['name'], getProfileUrl($user['id'], true));
                }
                $tbout = implode(', ', $names);
            }
            return (!empty($tbout)) ? $tbout : __('No birthdays today',true,'users');
        };
        
        $markers['count_users'] = function() {
                return getAllUsersCount();
        };
        $markers['users_edit'] = function() {return (\ACL::turnUser(array('users', 'edit_users'))) ? '1' : '0';};
        
        
        
        $markers['admin_access'] = (\ACL::turnUser(array('__panel__', 'entry'))) ? '1' : '0';
        $markers['rss'] = function() use($obj) {return $obj->getRss();};
        $markers['users_groups'] = function() {
            $groups = \ACL::getGroups();
            $default_id = \ACL::getDefaultGroupId();
            foreach ($groups as $group_id => $group) {
                $groups[$group_id]['id'] = $group_id;
                $groups[$group_id]['default'] = ($group_id == $default_id);
            }
            return $groups;
        };
        
        
        
        $markers = \Events::init('after_parse_global_markers', $markers);

        return $markers;
    }

    /**
     * Save markers. Get list of markers
     * and content wthat will be instead markers
     * Before view this markers will be replaced in
     * all content
     *
     * @param array $markers - marker->value
     * @return none
     */
    protected function _globalize($markers = array())
    {
        $this->globalMarkers = array_merge($this->globalMarkers, $markers);
    }

    /**
    * @return     list with RSS links
    */
    protected function getRss()
    {
        $rss = '';
        $installed_modules = \ModuleManager::getInstalledModules();
        if (!empty($installed_modules) && is_array($installed_modules)) {
            foreach($installed_modules as $module) {
                $module_name = '\\' . ucfirst($m) . 'Module\ActionsHandler';
                if (\Config::read('active', $module) && \Config::read('rss_' . $module, 'rss') && method_exists($module_name, 'rss')) {
                    $rss .= get_img('/template/' . getTemplate() . '/img/rss_icon_mini.png') .
                        get_link(__(ucfirst($module) . ' RSS'), '/' . $module . '/rss/') .
                        '<br />';
                }
            }
        }
        return $rss;
    }



    /**
     * @return string
     *
     * Build menu which creating in Admin Panel
     */
    protected function builMainMenu()
    {
        $menu_conf_file = ROOT . '/sys/settings/menu.dat';
        if (!file_exists($menu_conf_file)) return false;
        $menudata = unserialize(file_get_contents($menu_conf_file));


        if (!empty($menudata) && count($menudata) > 0) {
            $out = $this->buildMenuNode($menudata, 'class="fpsMainMenu"');
        } else {
            return false;
        }
        return $out;
    }


    /**
     * @param  $node
     * @param string $class
     * @return string
     */
    protected function buildMenuNode($node, $class = 'class="fpsMainMenu"')
    {
        $out = '<ul ' . $class . '>';
        foreach ($node as $point) {
            if (empty($point['title']) || empty($point['url'])) continue;
            if (!empty($point['sub']) && count($point['sub']) > 0) {
               $subClass = 'class="fpsSubMenu"';
            } else {
               $subClass = '';
            }
            $out .= '<li ' . $subClass . '>';


            $out .= $point['prefix'];
            $target = (!empty($point['newwin'])) ? ' target="_blank"' : '';
            $out .= '<a href="' . get_url($point['url']) . '"' . $target . '>' . $point['title'] . '</a>';
            $out .= $point['sufix'];

            if (!empty($point['sub']) && count($point['sub']) > 0) {
                $out .= $this->buildMenuNode($point['sub']);
            }

            $out .= '</li>';
        }
        $out .= '</ul>';
        return $out;
    }



    /**
    * @param  mixed $tag
    * @return boolean
    */
    protected function setCacheTag($tag)
    {
        if ((\Config::read('cache') == true || \Config::read('cache') == 1) && $this->cached === true) {
            if (is_array($tag)) {
                foreach ($tag as $_tag) {
                    $this->setCacheTag($_tag);
                }
            } else {
                $this->cacheTags[] = $tag;
                return true;
            }
        }
        return false;
    }


    /**
    * create unique id for cache file
     *
    * @param array $params <module>[ action [ param1 [ param2 ]]]
    * @return string
    */
    private function getKeyForCache($params)
    {
        $cacheId = getTemplate() . '_';
        foreach ($params as $value) {
            if (is_array($value)) {
                foreach ($value as $_value) {
                    $cacheId .= $_value . '_';
                }
            } else {
                $cacheId .= $value . '_';
            }
        }

        if (!empty($_GET['page']) && is_numeric($_GET['page'])) {
            $cacheId .= intval($_GET['page']) . '_';
        }
        if (!empty($_GET['order'])) {
            $order = (string)$_GET['order'];
            if (!empty($order)) {
                $order = substr($order, 0, 10);
                $cacheId .= $order . '_';
            }
        }
        if (!empty($_GET['asc'])) {
            $cacheId .= 'asc_';
        }
        $cacheId .= (\UserAuth::getField('status')) ? \UserAuth::getField('status') : '0';
        return $cacheId;
    }

    /**
     * Building categories chain if necessary
     *
     * @param string $cat_id Category/categories ID
     * @return array
     */
    private function buildCategoriesChain($cat_id) {
        $chain = array();
        if (method_exists($this, 'getCategoriesChain')) {
            $chain = $this->getCategoriesChain($cat_id);
            foreach ($chain as $index => $node) {
                $new_item = array();
                if (isset($node['id'])) { // One category
                    $new_item['text'] = h($node['title']);
                    $new_item['link'] = get_link(h($node['title']), $node['url']);
                } elseif (is_array($node)) { // Many categories
                    $new_item['text'] = '';
                    $new_item['link'] = '';
                    foreach ($node as $category) {
                        $new_item['text'] .= ($new_item['text'] ? ', ' : '') . h($category['title']);
                        $new_item['link'] .= ($new_item['link'] ? ', ' : '') . get_link(h($category['title']), $category['url']);
                    }
                }
                $chain[$index] = $new_item;
            }
        }
        return $chain;
    }

    protected function buildBreadCrumbs($cat_id = false, $append = false) {
        $output = '';
        $chain = array();

        if ($append) {
            if (is_array($append)) {
                foreach ($append as $item) {
                    array_unshift($chain, $item);
                }
            } else {
                $chain[] = $append;
            }
        }
        
        $chain = array_merge($chain, $this->buildCategoriesChain($cat_id));

        if ($this->module != 'home') {
            $chain[] = array(
                'text' => h($this->module_title),
                'link' => get_link(h($this->module_title), $this->getModuleURL())
            );
        }
        if (\Config::read('skip_home_link') != 1) {
            $chain[] = array(
                'text' => h(__('Home')),
                'link' => get_link(h(__('Home')), '/')
            );
        }
        if (count($chain)) {
            if (\Config::read('skip_title_item') == 1) {
                array_shift($chain);
            } elseif (isset($chain[0]['text'])) {
                $chain[0] = $chain[0]['text'];
            }
            foreach ($chain as $index => $node) {
                if (isset($node['link'])) {
                    $chain[$index] = $node['link'];
                }
            }
            
            // Customize navigation if necessary
            $custom_file = ROOT . '/template/' . getTemplate() . '/customize/navigation.php';
            if (file_exists($custom_file)) {
                include_once($custom_file);
                if (function_exists('custom_navigation')) {
                    return call_user_func_array('custom_navigation', array($chain, $this->module));
                }
            }

            // Trim Breadcrumbs
            $max_breadcrumbs_length = \Config::read('max_breadcrumbs_length');
            if ($max_breadcrumbs_length > 0 && count($chain) >= $max_breadcrumbs_length) {
                $type_trim_items = \Config::read('type_trim_items');
                $skip = count($chain) - $max_breadcrumbs_length;
                switch ($type_trim_items) {
                    case 2:
                        for ($n = 0; $n < $skip; $n++) {
                            array_shift($chain);
                        }
                        break;
                    case 1:
                        for ($n = 0; $n < $skip; $n++) {
                            array_pop($chain);
                        }
                        break;
                    default:
                        $index = round($max_breadcrumbs_length / 2);
                        for ($n = 0; $n < $skip; $n++) {
                            unset($chain[$n+$index]);
                        }
                        break;
                }
            }
            foreach ($chain as $node) {
                $output = $node . ($output ? __('Separator') . $output : '');
            }
        }
        return $output;
    }

    /*
     *  Вспомогательная функция - после выполнения пользователем каких-либо действий
     *  выдает информационное сообщение и/или делает редирект на нужную страницу с задержкой
     *
     * $message - сообщение для информирования пользователя
     * $url - ссылка для редиректа или кнопки "продолжить"
     * $type - тип окошка:
     *  'ok' - делать редирект по url(успешное завершение запроса)
     *  'grand' - не делать редиректа, но выводить ссылку.(успешное завершение запроса)
     *  'error' - не делать редиректа, но выводить ссылку и ошибки
     *  'alert' или любой другой - делать редирект.(считается успешным завершением запроса)
     *
    */
    function showMessage($message, $url = false, $type = 'error', $notRoot=false)
    {
        // Запрещаем индексацию информационных страниц.
        header('X-Robots-Tag: noindex,nofollow');

        // Преобразуем URL
        $url = ($url == false) ? $this->getModuleURL() : $url;
        $url = (empty($url)) ? '/' : $url;
        $url = (used_https() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . get_url($url, $notRoot);

        // Если нужен сокращенный вид для ajax окошек
        if (isset($_GET['ajax']) && $_GET['ajax'] == true) {
            header('ResponseAjax: ' . ($type == 'alert' ? 'ok' : $type));
            $output = $this->render('infomessage.html.twig', array(
                'data' => array(
                    'message' => $message,
                    'url' => $url,
                    'status' => $type
                )
            ));

        // Если нужны только данные в формате json
        } elseif (isset($_GET['json'])) {
            die(json_encode(array(
                'message' => $message,
                'url' => $url,
                'status' => $type
            ), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));

        // Если за информацией обратилось не ajax окошко, то возвращаем полноценную информационную страницу
        } else {
            $output = $this->render('infomessagegrand.html.twig', array(
                'data' => array(
                    'message' => $message,
                    'url' => $url,
                    'status' => $type
                )
            ));
        }

        // Решаем делать ли редирект
        switch($type) {
            case 'grand': break;
            case 'error': break;
            case 'alert': break;
            default: // 'ok' или любое другое
                header('Refresh: ' . \Config::read('redirect_delay') . '; url='.$url);
                break;
        }
        echo $output;
        die();
    }


    // Функция возвращает путь к модулю
    protected function getModuleURL($page = null)
    {
        $url = '/' . $this->module . '/' . (!empty($page) ? $page : '');
        $url = AtmUrl::parseRoutes($url);
        return $url;
    }


    // Функция возвращает путь к файлам модуля
    protected function getFilesPath($file = null, $module = null)
    {
        if (!isset($module)) $module = $this->module;
        $path = '/data/files/' . $module . '/' . (!empty($file) ? $file : '');
        return $path;
    }


    /* Функция возвращает путь к изображениям модуля или папке с ними
     *
     * @param boolean $type - True (full) or False (small)
     * @param mixed $file
     * @param mixed $module
     * @return string.
     */
    protected function getImagesPath($file = null, $module = null, $size_x = null, $size_y = null)
    {
        if (!isset($module)) $module = $this->module;
        $path = '/data/images/' . $module . '/' . (($size_x !== null and $size_y !== null) ? $size_x . 'x' . $size_y . '/' : '') . (!empty($file) ? $file : '');
        return $path;
    }


    // Функция возвращает путь к временным файлам модуля
    protected function getTmpPath($file = null)
    {
        $path = '/sys/tmp/' . $this->module . '/' . (!empty($file) ? $file : '');
        return $path;
    }


    // Функция возвращает максимально допустимый размер файла
    protected function getMaxSize($param = 'max_file_size')
    {
        $max_size = \Config::read($param, $this->module);
        if (empty($max_size) || !is_numeric($max_size)) $max_size = \Config::read('max_file_size');
        if (empty($max_size) || !is_numeric($max_size)) $max_size = 1048576;
        return $max_size;
    }


    // Функция обрабатывает метку изображения
    protected function insertImageAttach($message, $filename, $number, $module = null)
    {
        if (!isset($module)) $module = $this->module;

        if (\Config::read('use_local_preview', $module)) {
            $preview = \Config::read('use_preview', $module);
            $size_x = \Config::read('img_size_x', $module);
            $size_y = \Config::read('img_size_y', $module);
        } else {
            $preview = \Config::read('use_preview');
            $size_x = \Config::read('img_size_x');
            $size_y = \Config::read('img_size_y');
        }

        $image_link = get_url($this->getImagesPath($filename, $module));
        $preview_link = $image_link;
        $sizes = ' style="max-width:' . ($size_x > 150 ? $size_x : 150) . 'px; max-height:' . ($size_y > 150 ? $size_y : 150) . 'px;"';

        if ($preview) {
            if (file_exists(ROOT.$this->getImagesPath($filename, $module, $size_x, $size_y))) {
                $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
            } else {
                // Узнаем, а нужно ли превью для изображения
                list($width, $height, $type, $attr) = @getimagesize(R . $image_link);
                if ((empty($width) and empty($height)) or ($width > $size_x or $height > $size_y)) {
                    $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
                } else {
                    $preview = false;
                    $sizes = '';
                }
            }
        }
        $str =
            ($preview ? '<a class="gallery" rel="example_group" href="' . $image_link . '">' : '') .
            '<img %s alt=""' . $sizes . ' src="' . $preview_link . '" />' .
            ($preview ? '</a>' : '');
        $from = array(
            '{IMAGE' . $number . '}',
            '{LIMAGE' . $number . '}',
            '{RIMAGE' . $number . '}',
        );
        $to = array(
            sprintf($str, 'class="textIMG"'),
            sprintf($str, 'align="left" class="LtextIMG"'),
            sprintf($str, 'align="right" class="RtextIMG"'),
        );
        return str_replace($from, $to, $message);
    }


    // Функция обрабатывает метку изображения в шаблоне
    protected function markerImageAttach($filename, $number, $module = null)
    {
        if (!isset($module)) $module = $this->module;
        $image_link = get_url($this->getImagesPath($filename, $module));
        return $image_link;
    }


    // Функция обрабатывает метку превью на изображение в шаблоне
    protected function markerSmallImageAttach($filename, $number, $module = null)
    {
        if (!isset($module)) $module = $this->module;
        if (isset($module) and \Config::read('use_local_preview', $module)) {
            $preview = \Config::read('use_preview', $module);
            $size_x = \Config::read('img_size_x', $module);
            $size_y = \Config::read('img_size_y', $module);
        } else {
            $preview = \Config::read('use_preview');
            $size_x = \Config::read('img_size_x');
            $size_y = \Config::read('img_size_y');
        }
        $image_link = get_url($this->getImagesPath($filename, $module));
        $preview_link = $image_link;

        if ($preview) {
            if (file_exists(R.'/'.$this->getImagesPath($filename, $module, $size_x, $size_y))) {
                $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
            } else {
                // Узнаем, а нужно ли превью для изображения
                list($width, $height, $type, $attr) = @getimagesize(R . $image_link);
                if ((empty($width) and empty($height)) or ($width > $size_x or $height > $size_y)) {
                    $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
                }
            }
        }

        return $preview_link;
    }


    /**
     * Если материал был просмотрен, то возврат true.
     * Если не был просмотрен, то запись в сессию и возврат false
     *
     * @param int $id
     * @param string $module
     * @return boolean
     */
    protected function material_are_viewed($id = null, $module = null)
    {
        $id = (int)$id;
        if ( $id < 1 ) return false;
        if ( !isset($module) ) $module = $this->module;

        if ( !isset($_SESSION[$module]) or
             !isset($_SESSION[$module]["materials_are_viewed"]) or
             !array_key_exists($id, $_SESSION[$module]["materials_are_viewed"]) )
        {
            $_SESSION[$module]["materials_are_viewed"][$id] = null;
            return false;
        }

        return true;
    }
}
