<?php
/**
* @project    Atom-M CMS
* @package    Additional Fields Entity
* @url        https://atom-m.net
*/

namespace Traits;

trait AdditionalFieldsEntity {

    /**
    * Вычленияет из массива полей доп. поля.
    */
    private function getAdditionalFieldsValues() {
        $values = array();
        foreach($this->asArray() as $key => $value) {
            if (strpos($key, 'add_field_') === 0) {
                $values[$key] = $value;
            }
        }
        return $values;
    }
}
