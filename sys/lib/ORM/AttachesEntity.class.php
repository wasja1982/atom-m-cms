<?php
/**
* @project    Atom-M CMS
* @package    Attaches Entity
* @url        https://atom-m.net
*/


namespace ORM;

class AttachesEntity extends \OrmEntity
{

    protected $id;
    protected $entity_id;
    protected $user_id;
    protected $attach_number;
    protected $filename ;
    protected $size;
    protected $date;
    protected $is_image;
    protected $module;


    public function save()
    {
        $params = array(
            'entity_id' => intval($this->entity_id),
            'user_id' => intval($this->user_id),
            'attach_number' => intval($this->attach_number),
            'filename' => $this->filename,
            'size' => intval($this->size),
            'date' => $this->date,
            'is_image' => (!empty($this->is_image)) ? '1' : new \Expr("'0'"),
            'module' => $this->module,
        );
        if($this->id) $params['id'] = $this->id;

        return (getDB()->save('attaches', $params));
    }



    public function delete()
    {
        $path_files = ROOT . '/data/files/' . $this->module . '/' . $this->filename;
        $path_images = ROOT . '/data/images/' . $this->module . '/' . $this->filename;
        if (file_exists($path_files)) {
            unlink($path_files);
        } elseif (file_exists($path_images)) {
            unlink($path_images);
        }

        if (\Config::read('use_local_preview', $this->module)) {
            $preview = \Config::read('use_preview', $this->module);
            $size_x = \Config::read('img_size_x', $this->module);
            $size_y = \Config::read('img_size_y', $this->module);
        } else {
            $preview = \Config::read('use_preview');
            $size_x = \Config::read('img_size_x');
            $size_y = \Config::read('img_size_y');
        }
        $path = ROOT.'/data/images/' . $this->module . '/'.$size_x.'x'.$size_y.'/'.$this->filename;
        if (file_exists($path)) unlink($path);


        getDB()->delete('attaches', array('id' => $this->id));
    }


    public function getListKeys() {
        return array_keys(get_object_vars($this));
    }


    public function __getAPI() {

        if (
            !\ACL::turnUser(array($this->module, 'view_list')) ||
            !\ACL::turnUser(array($this->module, 'view_materials')) ||
            !\ACL::turnUser(array($this->module, 'download_files'))
        )
            return array();

        if (
            !\ACL::turnUser(array($this->module, 'view_forums_list')) ||
            !\ACL::turnUser(array($this->module, 'view_forums')) ||
            !\ACL::turnUser(array($this->module, 'view_themes')) ||
            !\ACL::turnUser(array($this->module, 'download_files'))
        )
            return array();
        
        return array(
            'id' => $this->id,
            'entity_id' => $this->entity_id,
            'user_id' => $this->user_id,
            'attach_number' => $this->attach_number,
            'filename' => $this->filename,
            'size' => $this->size,
            'date' => $this->date,
            'is_image' => $this->is_image,
            'module' => $this->module,
        );
    }
}
