<?php
/**
* @project    Atom-M CMS
* @package    Comments Model
* @url        https://atom-m.net
*/


namespace ORM;

class CommentsModel extends \OrmModel
{

    public $Table = 'comments';
    protected $RelatedEntities = array(
        'author' => array(
            'model' => 'Users',
            'type' => 'has_one',
            'foreignKey' => 'user_id',
          ),
        'parent_entity' => array(
            'model' => 'this.module',
            'type' => 'has_one',
            'foreignKey' => 'entity_id',
        ),
    );
}