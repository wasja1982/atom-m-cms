<?php
/**
* @project    Atom-M CMS
* @package    Attaches Model
* @url        https://atom-m.net
*/


namespace ORM;

class AttachesModel extends \OrmModel
{

    public $Table = 'attaches';

}