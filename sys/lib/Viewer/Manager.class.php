<?php
/**
* @project    Atom-M CMS
* @package    Viewer class
* @url        https://atom-m.net
*/


class Viewer_Manager
{

    protected $loader;

    protected $layout = 'default';

    private $markersData = array();

    protected static $twig_proxy;
    
    protected $template_path = '';
    


    public function __construct($loader = array())
    {
        require_once dirname(__FILE__).'/Twig/Autoloader.php';
        require_once(dirname(__FILE__) . '/TwigProxy.class.php');
        
        Twig_Autoloader::register(true);

        $this->layout = (!empty($loader['layout'])) ? $loader['layout'] : 'default';    
        
        $this->template_path = ROOT . '/template/' . getTemplate() . '/html/';

        $this->loader = new Twig_Loader_Filesystem($this->template_path);
        $this->setPluginsNamespace();
    }



    public function setLayout($layout)
    {
        $this->layout = trim($layout);
    }



    public function view($fileName, $context = array())
    {
        $filePath = $this->getTemplateFilePath($fileName);
        $cached = false;
        $filePath = str_replace(ROOT, '', $filePath);

        $start = getMicroTime();
        $data = $this->parseTemplate($fileName, $context, $cached);
        $took = getMicroTime() - $start;

        \AtmDebug::addRow(
            array('Templates', 'Compile time', 'Cached'),
            array($filePath, $took, ($cached ? 'From cache' : 'Compiled'))
        );

        return $data;
    }


    
    public function prepareContext($context)
    {
        $return = array_merge($this->markersData, $context);
        return \Events::init('markers_data', $return);
    }



    public function getTemplateFilePath($fileName, $relative=false)
    {
        $path = $this->template_path . '%s' . '/' . $fileName;

        if (file_exists(sprintf($path, $this->layout))) {
            $path = sprintf($path, $this->layout);
        } else {
            $path = sprintf($path, 'default');
        }

        if ($relative) {
            $path = str_replace($this->template_path, '', $path);
        }
        
        $path = preg_replace('#([\\/])+#', '\\1', $path);

        return $path;
    }



    public function parseTemplate($fileName, $context, &$cached = false)
    {
        try {

            $markers = $this->prepareContext($context);
            $twig_proxy = new TwigProxy($markers);
            
            $vals = array();
            $vals['atm'] = $twig_proxy;

            $twig = new Twig_Environment($this->loader);
            $twig->addFilter(new Twig_SimpleFilter('adate', function ($date, $format = false, $relative = true) {
                return AtmDate($date, $format, $relative);
            }, array('is_safe' => array('all'))));
            
            $twig_template = $twig->loadTemplate($this->getTemplateFilePath($fileName, true));

            return $twig_template->render($vals);

        } catch (Twig_Error_Syntax $e) {
            // template contains one or more syntax errors
            echo $e;
        }


    }

    

    public function setMarkers($markers)
    {
        $this->markersData = array_merge($this->markersData, $markers);
    }



    public function setPluginsNamespace() {
        $installed_plugins = \Config::read('installed_plugins');
        foreach ($installed_plugins as $plugin_name) {
            $this->loader->addPath(ROOT . '/plugins/' . $plugin_name . '/template/', $plugin_name);
        }
    }
}
