<?php
/**
* @project    Atom-M CMS
* @package    Plugins manager
* @url        https://atom-m.net
*/

include_once '../sys/boot.php';
include_once './inc/adm_boot.php';

class PluginsAdmin {

    static $source_url;

    public function __construct()
    {
        self::$source_url = 'http://api.atom-m.net/plugins-'.ATOM_VERSION[0];
    }

    /*
     * Главная страница менеджера
     */
    function index() {
        $pageTitle = __('Plugins manager');
        $pageNav = $pageTitle;
        $pageNavr = '<a class="waves-effect modal-trigger btn" href="#sec">' . __('Include of the archive') . '</a>';
        include_once ROOT . '/admin/template/header.php';
        $content = '';

        if (!extension_loaded( "openssl" )) {
            $content .= '<blockquote>'.__('Requires php_openssl').'</blockquote>';
        }

        $content .= '

            <style>
                #plugins li {
                    min-height: 84px;
                    height: auto;
                    max-height: 150px;
                    padding-left: 93px;
                    padding-right: 189px;
                    border-left: 15px solid #fff;
                }
                #plugins li img {
                    width: 64px;
                    height: 64px;
                    border-radius: 2px;
                }
                #plugins li.on {
                    border-left: 15px solid #4caf50;
                }
                #plugins li.off {
                    border-left: 15px solid #cecece;
                }
                #plugins li.new {
                    border-left: 15px solid #ff0000;
                }
                #plugins .secondary-content {
                    font-size: 1.5em;
                }
                #plugins .switch {
                    float: right;
                }
                #plugins .nya {
                    display: inline-block;
                    padding-top: 4px;
                    padding-right: 15px;
                }
                #plugins li .secondary-content a {
                    padding-left: 10px;
                }
            </style>

            <div id="sec" class="modal modal-fixed-footer">
                <form id="locsend" method="post" action="plugins.php?ac=local" onSubmit="locsend(this); return false" enctype="multipart/form-data">
                    <div class="modal-content">
                        <h4>' . __('Include of the archive (Only ZIP)') . '</h4>
                        <div class="input-field col s12">
                            <input id="pl_url" type="text" name="pl_url" placeholder="http://site.com/path/to/plugin.zip" />
                            <label for="pl_url">' . __('Load plugin with remote server') . '</label>
                        </div>
                        <div class="file-field input-field col s12">
                        <input class="file-path" placeholder="' . __('Load plugin as local file') . '" type="text"/>
                        <div class="btn">
                            <span>ФАЙЛ</span>
                            <input  type="file" accept="application/zip" name="pl_file" onChange="if (pl_file.value.substring(pl_file.value.lastIndexOf(\'.\')+1, pl_file.value.length).toLowerCase() != \'zip\')
                                { Materialize.toast(\'File type should be ZIP\', 4000); return; }" />
                        </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-action waves-effect modal-close btn-flat">'.__('Cancel').'</a>
                        <input type="submit" value="' . __('Include') . '" name="send" class="btn" />
                    </div>
                </form>
            </div>

            <div id="popup" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h4>' . __('Include') . '</h4>
                    <div id="po_cont">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action waves-effect modal-close btn-flat">'.__('Cancel').'</a>
                </div>
            </div>

            <ul id="chswitch" class="tabs">
                <li class="aviable tab col s3"><a href="#" class="active">' . __('Aviable') . '</a></li>
                <li class="installed tab col s3"><a class="active" href="#">' . __('Installed') . '</a></li>
                <li class="updates tab col s3"><a class="active" href="#">' . __('Updates') . '</a></li>
            </ul>
            <div id="main" style="position:relative; padding-bottom: 100px">
                <ul id="plugins" class="collection">
                </ul>
            </div>

            <script>

                function load() {
                    var preloader = \'<div class="progress"><div class="indeterminate"></div></div>\';
                    var timer = setTimeout(function(){
                        $(\'#plugins\').html(preloader)
                    }, 500);
                    type = \'aviable\';
                    if ($(\'#chswitch .installed\').hasClass(\'act\')) {
                        type = \'installed\';
                    }
                    if ($(\'#chswitch .updates\').hasClass(\'act\')) {
                        type = \'updates\';
                    }
                    $.ajax({
                        url: "/admin/plugins.php",
                        data: "ac=list&type="+type,
                        cache: false,
                        success: function(html){
                            clearTimeout(timer);
                            $("#plugins").html(html);
                        },
                        error: function(response) {
                            clearTimeout(timer);
                            Materialize.toast(response, 4000);
                        }
                    });
                }

                function clear() {
                    $(\'#chswitch .aviable\').removeClass(\'act\');
                    $(\'#chswitch .installed\').removeClass(\'act\');
                    $(\'#chswitch .updates\').removeClass(\'act\');
                }
                $(\'#chswitch .aviable\').click(function() {
                    clear();
                    $(\'#chswitch .aviable\').addClass(\'act\');
                    load();
                });
                $(\'#chswitch .installed\').click(function() {
                    clear();
                    $(\'#chswitch .installed\').addClass(\'act\');
                    load();
                });
                $(\'#chswitch .updates\').click(function() {
                    clear();
                    $(\'#chswitch .updates\').addClass(\'act\');
                    load();
                });

                $(\'#chswitch .aviable\').click();

                function openPopupNew(e) {
                    $(\'.modal-content\').html("<div class=\"items\">' . __('Please, wait...') . '</div>");
                    $(\'#popup\').openModal();
                    jQuery.ajax({
                        url:     $(e).attr("href"),
                        type:     "POST",
                        dataType: "html",
                        data: jQuery(e).serialize(), 
                        success: function(response) {
                            $(\'.modal-content\').html(response);
                            load();
                        },
                        error: function(response) {
                            $(\'.modal-content\').html("' . __('Some error occurred') . '");
                        }
                    }); 
                }

                function asend(e) {
                    jQuery.ajax({
                        url:     $(e).attr("href"),
                        type:     "POST",
                        dataType: "html",
                        data: jQuery(e).serialize(), 
                        success: function(response) {
                            load();
                        },
                        error: function(response) {
                            $(\'#po_cont\').html("<div class=\"items\">' . __('Some error occurred') . '</div>");
                            $(\'#popup\').openModal();
                        }
                    }); 
                }

                function locsend(e) {
                    $(\'#sec\').closeModal();
                    
                    $(\'#po_cont\').html("<div class=\"items\">' . __('Please, wait...') . '</div>");
                    $(\'#popup\').openModal();

                    form = document.forms.locsend;
                    formData = new FormData(form);
                    xhr = new XMLHttpRequest();
                    xhr.open("POST", $(e).attr("action"));

                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4) {
                                data = xhr.responseText;
                                $(\'#po_cont .items\').html(data);
                                load();
                        }
                    };
                    xhr.send(formData);

                }
                
                $(document).ready(function(){
                    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
                    $(\'.modal-trigger\').leanModal();
                });

            </script>
        ';

        echo $content;
        include_once ROOT . '/admin/template/footer.php';
    }


    /*
     * Показ списка плагинов
     * 
     * $type - вкладка для отображения
     */
    function showPluginsList($type) {
        $content = '';
        $new_plugins = \PluginManager::getNewPlugins();
        $installed_plugins = \PluginManager::getInstalledPlugins();

        $Cache = new Cache;
        $Cache->lifeTime = 6000;
        if ($Cache->check('adm_pl_list')) {
            $source_list_raw = $Cache->read('adm_pl_list');
        } else {
            $source_list_raw = file_get_contents(self::$source_url);
            $Cache->write($source_list_raw, 'adm_pl_list', array());
        }
        
        if ($type == 'aviable') {
            $plugins = json_decode($source_list_raw, true);
            if (count($plugins) < 1) {
                echo $content .= '<div class="warning">' . __('Catalog plugins not found') . '</div>';
                return;
            }
        } elseif ($type == 'installed') {
            $plugins = array_merge($installed_plugins, $new_plugins);
        } elseif ($type == 'updates') {

            $all_plugins = json_decode($source_list_raw, true);
            if (count($all_plugins) < 1) {
                echo $content .= '<div class="warning">' . __('Catalog plugins not found') . '</div>';
                return;
            }
                
            $plugins = array_merge($installed_plugins, $new_plugins);
            // перебор списка имеющихся плагинов, поиск их в каталоге
            // и если отличаются версии, то запись в массив данных о версии плагина из каталога
            foreach ($plugins as $k => $name) {
                foreach ($all_plugins as $r => $result) {
                    if (strtolower($result['name']) == $name) {

                        $local_verion = \PluginManager::getPluginVersion($name);
                        if ($local_verion != $result['version']) {
                            $plugins[$k] = $result;
                            $plugins[$k]['local_version'] = $local_verion;
                        }

                        break;
                    }
                }
                if (!is_array($plugins[$k])) {
                    unset($plugins[$k]);
                }
                if (count($plugins) < 1) {
                    echo $content .= '<div class="warning">' . __('All plugins up to date') . '</div>';
                    return;
                }
            }
        } else {
            return;
        }

        $allow_install = true;
        if (!extension_loaded( "openssl" )) $allow_install = false;

        foreach ($plugins as $result) {
            $plugin = ($type == 'installed') ? $result : strtolower($result['name']);
            $plugin_exist = (in_array($plugin, $installed_plugins) || in_array($plugin, $new_plugins));
            $plugin_info = $plugin_exist ? json_decode(file_get_contents(\PluginManager::getConfigFile($plugin)), 1) : $result;
            
            $buttoms = '<div class="secondary-content">';
            $class = '';
            $icon = '';
            if (!empty($plugin_info['more'])) {
                $buttoms .= '<a href="' . h($plugin_info['more']) . '"><i class="mdi-action-language small"></i></a>';
            }
 
            if ($plugin_exist) {
                
                if ($type == 'updates') {
                
                    $buttoms .= '<a title="' . __('Update') . '" href="plugins.php?ac=update&url=' . $result['path_zip'] . '&dir=' . $plugin . '&version=' . $result['version'] . '" onclick="openPopupNew(this);return false"><i class="material-icons small">update</i></a>';
                
                } elseif (in_array($plugin, $installed_plugins)) {
                    
                    $buttoms .= '
                        <div class="switch">
                        <label>
                        Off
                        <input type="checkbox" '.(!empty($plugin_info['active']) ? "checked" : "").' href="plugins.php?ac='.(!empty($plugin_info['active']) ? "off" : "on").'&dir=' . $plugin . '" onchange="asend(this);return false">
                        <span class="lever"></span>
                        On
                        </label>
                    </div>';
                
                    if (!empty($plugin_info['active'])) {
                        if (file_exists(\PluginManager::getSettingsFile($plugin))) {
                            $buttoms .= '<div class="nya"><a title="' . __('Edit') . '" href="plugins.php?ac=edit&dir=' . $plugin . '"><i class="mdi-action-settings small"></i></a></div>';
                        }
                        $class = 'on';
                    } else {
                        $buttoms .= '<div class="nya"><a title="' . __('Uninstall') . '" href="plugins.php?ac=uninstall&dir=' . $plugin . '" onclick="asend(this);return false"><i class="mdi-action-highlight-remove small"></i></a></div>';
                        $class = 'off';
                    }

                } else {
                    $buttoms .= '<a title="' . __('Install') . '" href="plugins.php?ac=install&dir=' . $plugin . (isset($result['version']) ? '&version=' . $result['version'] : '') . '" onclick="asend(this);return false"><i class="mdi-action-get-app small"></i></a>';
                    $class = 'new';
                }
                if (!empty($plugin_info['icon'])) {
                    $icon_data = base64_encode(file_get_contents(\PluginManager::getPluginPath($plugin) . $plugin_info['icon']));
                    $size = getimagesize(\PluginManager::getPluginPath($plugin) . $plugin_info['icon']);
                    $icon = "<img class=\"circle\" src=\"data: " . $size['mime'] . ";base64," . $icon_data . "\">";
                }
            } else {
                if ($allow_install) {
                    $buttoms .= '<a href="plugins.php?ac=install&url=' . $result['path_zip'] . '&dir=' . $plugin . '&version=' . $result['version'] . '" title="' . __('Install') . '" onclick="openPopupNew(this);return false"><i class="mdi-action-get-app small"></i></a>';
                }
                if (!empty($plugin_info['icon'])) {
                    $path_icon = $result['path_raw'] . $plugin_info['icon'];
                    $icon = "<img class=\"circle\" src=\"" . h($path_icon) . "\">";
                }
            }
            $buttoms .= '</div>';

            $content .= '<li class="collection-item avatar '.$class.'">';
            $content .= $icon;
            $content .= "<span class=\"card-title black-text\">".h($plugin_info['title'])."</span>";
            $content .= "<p class=\"desc\">".h((empty($plugin_info['desc']) ? __('Description is not') : $plugin_info['desc'])).'</p>';
            $content .= $buttoms;

            $content .= '</li>';
        }

        echo $content;
    }


    /*
     * Включение и выключение плагина
     * 
     * $status - новый статус плагина,
     * $plugin - название плагина
     */
    function switchPlugin($status, $plugin) {
        if ($status) {
            \PluginManager::enablePlugin($plugin);
        } else {
            \PluginManager::disablePlugin($plugin);
        }

        $Cache = new Cache;
        $Cache->remove('adm_pl_settings');

        redirect('../admin/plugins.php');
    }


    /*
     * Вывод настроек плагина
     * 
     * $plugin - название плагина
     */
    function editPlugin($plugin) {
        if (empty($plugin)) redirect('/admin/plugins.php');
        if (!preg_match('#^[\w\d_-]+$#i', $plugin)) redirect('/admin/plugins.php');

        if (!file_exists(\PluginManager::getSettingsFile($plugin))){
            echo __('No settings for this plugin');
            return;
        }

        $config = json_decode(file_get_contents(\PluginManager::getConfigFile($plugin)), 1);

        $pageTitle = __('Manage plugins');
        $pageNav = '<a href="'.WWW_ROOT.'/admin/plugins.php">' . __('Return to manage plugins') . '</a>';
        $pageNavr = __('Manage plugin') . ' ' . h($config['title']);
        include_once ROOT . '/admin/template/header.php';
        include_once \PluginManager::getSettingsFile($plugin);
        print (!empty($output)) ? $output : '';
        include_once ROOT . '/admin/template/footer.php';
    }


    /*
     * Установка плагина из загруженного архива
     */
    function receivePlugin() {
        // local plugin archive
        if (!empty($_FILES['pl_file']['name'])) {

            // download plugin to tmp folder
            $plugin = \PluginManager::receivePlugin('pl_file');
            if (!$plugin) {
                echo \PluginManager::getErrors();
                return;
            }
            echo $this->installPlugin($plugin);
        } else if (!empty($_POST['pl_url'])) {
            echo $this->downloadPlugin($_POST['pl_url']);
        }
    }


    /*
     * Установка плагина по url адресу
     * 
     * $url - URL ZIP-архива
     * $plugin - название плагина
     */
    function downloadPlugin($url, $plugin, $version) {
        // download plugin to tmp folder
        $temp_name = \PluginManager::downloadPlugin($url, $plugin);
        if (!$temp_name) {
            echo \PluginManager::getErrors();
            return;
        }

        echo $this->installPlugin($temp_name, $version);
    }


    /*
     * Установка и включение плагина
     * 
     * $plugin - название плагина
     */
    function installPlugin($plugin, $version=none) {
        // install plugin
        $result = \PluginManager::installPlugin($plugin, $version);
        if (!$result) {
            echo \PluginManager::getErrors();
            return;
        }

        $files = \PluginManager::getPluginFiles($plugin);
        $message = __('Plugin is install');
        $message .= '<strong>' . __('New files') . '</strong><ul class="wps-list">';
        foreach ($files as $file) {
            $message .= '<li>' . $file . '</li>';
        }
        $message .= '</ul>';

        $Cache = new Cache;
        $Cache->remove('adm_pl_settings');

        echo $message;
    }


    /*
     * Удаление плагина
     * 
     * $plugin - название плагина
     */
    function uninstallPlugin($plugin, $purge=true) {
        \PluginManager::uninstallPlugin($plugin, $purge);

        $Cache = new Cache;
        $Cache->remove('adm_pl_settings');

        if ($purge) {
            redirect('../admin/plugins.php');
        }
    }


    /*
     * Обновление плагина
     * 
     * $url - URL ZIP-архива
     * $plugin - название плагина
     */
    function updatePlugin($url, $plugin, $version) {
    
        $this->uninstallPlugin($plugin, false);
    
        // download plugin to tmp folder
        $temp_name = \PluginManager::downloadPlugin($url, $plugin);
        if (!$temp_name) {
            echo \PluginManager::getErrors();
            return;
        }

        //
        $this->installPlugin($temp_name, $version);
    }
}

$PluginsAdmin = new PluginsAdmin();

if (!isset($_GET['ac'])) $_GET['ac'] = 'index';
$actions = array('index', 'list', 'install', 'uninstall', 'updatePlugin', 'on', 'off', 'edit', 'local');

switch ($_GET['ac']) {
    case 'list':
        $PluginsAdmin->showPluginsList($_GET['type']);
        break;
    case 'install':
        if (isset($_GET['url'])) {
            $PluginsAdmin->downloadPlugin($_GET['url'], $_GET['dir'], $_GET['version']);
        } else {
            $PluginsAdmin->installPlugin($_GET['dir']);
        }
        break;
    case 'uninstall':
        $PluginsAdmin->uninstallPlugin($_GET['dir']);
        break;
    case 'update':
        $PluginsAdmin->updatePlugin($_GET['url'], $_GET['dir'], $_GET['version']);
        break;
    case 'on':
        $PluginsAdmin->switchPlugin(1, $_GET['dir']);
        break;
    case 'off':
        $PluginsAdmin->switchPlugin(0, $_GET['dir']);
        break;
    case 'edit':
        $PluginsAdmin->editPlugin($_GET['dir']);
        break;
    case 'local':
        $PluginsAdmin->receivePlugin();
        break;
    default:
        $PluginsAdmin->index();
}

?>
