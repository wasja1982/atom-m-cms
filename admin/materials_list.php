<?php
/**
* @project    Atom-M CMS
* @package    Materials list
* @url        https://atom-m.net
*/

include_once '../sys/boot.php';
include_once ROOT . '/admin/inc/adm_boot.php';
$pageTitle = __('Materials list');
$Register = Register::getInstance();


$allowed_actions = array('delete', 'index', 'premoder');
if (empty($_GET['m']) || !\Config::read($_GET['m'].".std_admin_pages.materials_list")) {
    $_SESSION["message"][] = __('Some error occurred');
    redirect('/admin/');
}
$module = $_GET['m'];
$Register['module'] = $module;

$action = (!empty($_GET['ac'])) ? $_GET['ac'] : 'index';
if (empty($action) && !in_array($action, $allowed_actions)) $action = 'index';


$Controll = new MaterialsList;
list($output, $pages) = $Controll->{$action}($module);






class MaterialsList {
    public $pageTitle;
    
    public function __construct() {
        $this->pageTitle = __('Materials list');
    }


    public function index($module) {
        $output = '';
        $model = OrmManager::getModelInstance($module);
        
        $where = (!empty($_GET['premoder'])) ? array('premoder' => 'nochecked') : array();

        $total = $model->getTotal(array('cond' => $where));

        // количество материалов на странице
        $perPage = 20;

        list($pages, $page) = pagination($total, $perPage, WWW_ROOT.'/admin/materials_list.php?m=' . $module);

        $model->bindModel('categories');
        $model->bindModel('author');
        $materials = $model->getCollection($where, array(
            'page' => $page,
            'limit' => $perPage,
            'order' => 'date DESC',
        ));

        if (empty($materials)) {
            $output = '<div class="collection-item"><b>' . __('Materials not found') . '</b></div>';
        } else {

            $output .= '
                <table class="bordered">
                    <thead>
                        <tr>
                            <th data-field="title">' . __('Title') . '</th>
                            <th data-field="category">' . __('Category') . '</th>
                            <th data-field="status">' . __('Status') . '</th>
                            <th data-field="author">' . __('Author') . '</th>
                            <th data-field="date">' . __('Date') . '</th>
                            <th data-field="views">' . __('Views') . '</th>
                            <th data-field="activity">' . __('Activity') . '</th>
                        </tr>
                    </thead>
                    <tbody>';

            foreach ($materials as $mat) {
                $category = $mat->getCategories();

                $output .= '<tr>';

                $output .= '<td><a class="truncate" href="' . get_url(entryUrl($mat, $module)) . '"  title="' . h($mat->getTitle()) . '">' . h($mat->getTitle()) . '</a></td>';

                $output .= '<td>';
                foreach($category as $n => $cat)
                {
                    $output .= ($n !== 0 ? ', ' : '').'<a class="truncate" href="' . get_url($module . '/category/' . $cat->getId()) . '"  title="' . h($cat->getTitle()) . '">' . h($cat->getTitle()) . '</a>';
                }
                $output .= '</td>';

                $status_icon = 'mdi-alert-error';
                if ( ($mat->getAvailable() or $mat->getAvailable()===null) and $mat->getPremoder()=='confirmed' )
                {
                    $status_icon = 'mdi-action-visibility';
                }
                elseif ( (!$mat->getAvailable() or $mat->getAvailable()===null) and $mat->getPremoder()=='confirmed' )
                {
                    $status_icon = 'mdi-action-visibility-off';
                }
                elseif ( $mat->getPremoder()=='nochecked' )
                {
                    $status_icon = 'mdi-action-schedule';
                }
                elseif ( $mat->getPremoder()=='rejected' )
                {
                    $status_icon = 'mdi-navigation-close';
                }
                $output .= '<td><i class="' . $status_icon . ' small"></i></td>';

                $output .= '<td><a class="truncate" href="' . getProfileUrl($mat->getAuthor()->getId()) . '" title="' . h($mat->getAuthor()->getName()) . '">' . h($mat->getAuthor()->getName()) . '</a></td>';

                $output .= '<td>' . $mat->getDate() . '</td>';

                $output .= '<td>' . $mat->getViews() . '</td>';

                $output .= '<td>';
                if (!empty($_GET['premoder']))
                {
                    $output .= '
                    <a class="btn green" href="' . get_url('/admin/materials_list.php?m=' . $module . '&ac=premoder&status=confirmed&id=' . $mat->getId()) . '" class="on"><i class="mdi-action-done small"></i></a>
                    <a class="btn red" href="' . get_url('/admin/materials_list.php?m=' . $module . '&ac=premoder&status=rejected&id=' . $mat->getId()) . '" class="off"><i class="mdi-navigation-close small"></i></a>';
                }
                else
                {
                    $output .= '
                    <a class="btn green" href="' . get_url($module . '/edit_form/' . $mat->getId()) . '"><i class="mdi-action-settings small"></i></a>
                    <a class="btn red" href="' . get_url('/admin/materials_list.php?m=' . $module . '&ac=delete&id=' . $mat->getId()) . '" onClick="return _confirm();"><i class="mdi-action-delete small"></i></a>';
                }
                $output .= '</td>';

                $output .= '</tr>';
            }

            $output .= '</tbody>
                    </table>';

        }
        
        return array($output, $pages);
    }
    
    
    function premoder($module){
        $Model = OrmManager::getModelInstance($module);
        $entity = $Model->getById(intval($_GET['id']));
        if (!empty($entity)) {
            
            $status = $_GET['status'];
            if (!in_array($status, array('rejected', 'confirmed'))) $status = 'nochecked';
            
            $entity->setPremoder($status);
            $entity->save();
            $_SESSION['message'][] = __('Saved');
            
            
            //clean cache
            $Cache = new \Cache('pages');
            $Cache->clean(CACHE_MATCHING_ANY_TAG, array('module_' . $module));
        } else {
            $_SESSION['message'][] = __('Some error occurred');
        }
        redirect('/admin/materials_list.php?m=' . $module . '&premoder=1');
    }
    
    
    public function delete($module) {
        
        $model = OrmManager::getModelInstance($_GET['m']);
        $id = intval($_GET['id']);
        $entity = $model->getById($id);
        
        if (!empty($entity)) {
            $entity->delete();
            $_SESSION['message'][] = __('Material has been delete');
        }
        
        redirect(getReferer(), true);
    }
}




$pageNav = $Controll->pageTitle;
$pageNavr = $pages;
include_once ROOT . '/admin/template/header.php';
?>



<form method="POST" action="" enctype="multipart/form-data">
<div class="row">
    <div class="col s12">
        <div class="collection b-none">
            <?php echo $output; ?>
        </div>
    </div>
</div>
</form>








<?php include_once 'template/footer.php'; ?>
