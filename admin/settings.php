<?php
/**
* @project    Atom-M CMS
* @package    Admin Panel module
* @url        https://atom-m.net
*/


include_once '../sys/boot.php';
include_once R.'admin/inc/adm_boot.php';
$pageTitle = __('Common settings');
$Register = Register::getInstance();


// Get current module(group of settings)
if (empty($_GET['m']) || !is_string($_GET['m'])) $_GET['m'] = 'sys';
$module = trim($_GET['m']);

$Register['module'] = $module;
$config = \Config::read('all', $module);

// Список страниц настройки cms, генерирующихся автоматически

$sysMods = (isset($config['system_modules'])) ? $config['system_modules'] : array();
unset($sysMods['__db__']);


/**
 * For show template preview
 *
 * @param string $template
 * @return string
 */
function getImgPath($template) {
    $path = ROOT . '/template/' . $template . '/screenshot.png';
    if (file_exists($path)) {
        return get_url('/template/' . $template . '/screenshot.png');
    }
    return get_url('/data/img/noimage.jpg');
}

// Подготавливаем необходимые данные
if (in_array($module, $sysMods)) {

    switch($module) {
        case '__sys__':


            // Prepare templates select list
            $sourse = glob(ROOT . '/template/*', GLOB_ONLYDIR);
            if (!empty($sourse) && is_array($sourse)) {
                $templates = array();
                foreach ($sourse as $dir) {
                    if (preg_match('#.*/(\w+)$#', $dir, $match)) {
                        $templates[] = $match[1];
                    }
                }
            }
            $templateSelect = array();
            if (!empty($templates)) {
                foreach ($templates as $value) {
                    $templateSelect[$value] = ucfirst($value);
                }
            }



            // Генерация списка доступных для выбора языков
            $langSelect = array();
            $langs = getPermittedLangs();
            foreach ($langs as $lang) $langSelect[$lang] = $lang;



            // Prepare smiles select list
            $smiles = glob(ROOT . '/data/img/smiles/*/info.php');
            $smilesSelect = array();
            if (!empty($smiles)) {
                sort($smiles);
                foreach ($smiles as $value) {
                    if (is_file($value)) {
                        include_once $value;
                        $path = dirname($value);
                        $pos = strrpos($path, "/");
                        if ($pos >= 0) {
                            $value = substr($path, $pos + 1);
                        }
                        if (isset($smilesInfo) && isset($smilesInfo['name'])) {
                            $smilesSelect[$value] = $smilesInfo['name'];
                        };
                    }
                }
            } else {
                $smilesSelect['atomm'] = 'Atom-M';
            }




            break;
        case '__home__':
            $pageTitle = __('Homepage settings');

            // Получение списка модулей, доступных на главной
            $home_modules = array();
            $installed_modules = \ModuleManager::getInstalledModules();
            if (!empty($installed_modules) && is_array($installed_modules)) {
                foreach($installed_modules as $m) {
                    $model_name = \OrmManager::getModelName($m);
                    if (method_exists($model_name, '__getQueryForHome')) {
                        $home_modules['sub_' . $m] = array(
                            'type' => 'checkbox',
                            'title' => __($m,false,$m),
                            'value' => $m,
                            'fields' => 'latest_on_home',
                        );
                    }
                }
            }
            break;
        case '__rss__':
            $pageTitle = __('RSS settings');

            // Получение списка модулей, работающих с RSS
            $rss_modules = array();
            $installed_modules = \ModuleManager::getInstalledModules();
            if (!empty($installed_modules) && is_array($installed_modules)) {
                foreach($installed_modules as $m) {
                    $moduleName = '\\' . ucfirst($m) . 'Module\ActionsHandler';
                    if (method_exists(ucfirst($moduleName), 'rss')) {
                        $rss_modules['rss_' . $m] = array(
                            'type' => 'checkbox',
                            'title' => __($m,false,$m),
                        );
                    }
                }
            }
            break;
        case '__hlu__':
            $pageTitle = __('SEO settings');
            break;
        case '__sitemap__':
            $pageTitle = __('Sitemap settings');
            break;
        case '__secure__':
            $pageTitle = __('Security settings');
            break;
        case '__preview__':
            $pageTitle = __('Preview settings');
            break;
        case '__watermark__':
            $pageTitle = __('Watermark settings');

            // Prepare fonts select list
            $fonts = glob(ROOT . '/data/fonts/*.ttf');
            $fontSelect = array();
            if (!empty($fonts)) {
                sort($fonts);
                foreach ($fonts as $value) {
                    $pos = strrpos($value, "/");
                    if ($pos >= 0) {
                        $value = substr($value, $pos + 1);
                    }
                    $fontSelect[$value] = $value;
                }
            }

            break;
    }



    /* properties for system settings and settings that not linked to module
    * returns
    * $settingsInfo - настройки системных модулей
    * $noSub - Модули, настройки которых не обьеденены под один ключ.
    *
    */
    include_once R.'sys/settings/conf_properties.php';


    $settingsInfo = isset($settingsInfo[$module]) ? $settingsInfo[$module] : array();
} else {
    if (\ModuleManager::checkInstallModule($module)) {
        $pathToModInfo = \ModuleManager::getSettingsFile($module);
        $pageTitle = __($module) . ' - ' . __('Settings of module');
        if (file_exists($pathToModInfo)) {
            include ($pathToModInfo);
        } else {
            $settingsInfo = array(
                'title' => array(
                    'title' => __('Title'),
                    'description' => __('Title: info'),
                ),
                'description' => array(
                    'title' => __('Meta-Description'),
                    'description' => __('Meta-Description: info'),
                ),

                __('Other'),

                'active' => array(
                    'type' => 'checkbox',
                    'title' => __('Module status'),
                    'description' => __('Module status: info'),
                ),
            );
        }
    } else {
        $_SESSION['message'][] = sprintf(__('Module "%s" not found'), $module);
    }
}





// Save settings

if (isset($_POST['send']) && isset($settingsInfo)) {
    // Запоминаем текущее значение конфига.
    // Если мы настраиваем модуль, то запоминаем только значения этого модуля
    // Если это глобальные настройки, то запоминаем весь конфиг
    $tmpSet = (in_array($module, $sysMods) && !in_array($module, $noSub)) ? $config[$module] : $config;

    if ($module == '__home__') {
        $Cache = new \Cache('pages');
        $Cache->clean(CACHE_MATCHING_TAG, array('module_home', 'action_index'));
    }
    
    foreach ($settingsInfo as $fname => $params) {
        // Если это поле, которое нельзя изменять, то идем дальше, конфиг менять не нужно
        if (!empty($params['attr']) && !empty($params['attr']['disabled'])) continue;

        // Удаляем переменную, если она есть, чтобы небыло проблем.
        unset($value);

        // Узнаем, вложенная ли это настройка(вложена, это значит, что она обьеденена с другими настройками одним ключом, указанным в поле fields)
        if (!empty($params['fields'])) {
            // Если имя настройки начинается с sub_, значит следует формировать имя без приставки sub_
            if (false !== strpos($fname, 'sub_')) $fname = mb_substr($fname, mb_strlen('sub_'));
            // Если в данных для сохранения есть данные для этой настройки(измененные или нет, но не пустые)
            if (!empty($_POST[$params['fields']][$fname])) {
                // Меняем содержимое настройки в конфиге на то, что указано в форме
                $tmpSet[$params['fields']][$fname] = $_POST[$params['fields']][$fname];
            // Если настройка пустая, значит она не заполнена
            } else {
                // Если существует такой массив вложенных настроек
                if (isset($tmpSet[$params['fields']]) && is_array($tmpSet[$params['fields']])) {
                    // И в него входит текущая настройка
                    if (array_key_exists($fname, $tmpSet[$params['fields']]))
                        // Удаляем её, чтобы не занимала место.
                        unset($tmpSet[$params['fields']][$fname]);
                }
            }
            // Идем дальше, текущая настройка обработана
            continue;
        }


        // Если настройка существует, то запоминаем её значение
        if (isset($_POST[$fname]) || isset($_FILES[$fname])) {
            $value = trim((string)$_POST[$fname]);
        }



        if (empty($value)) $value = '';
        // Если тип checkbox, то ровняем либо 0 либо 1
        if (isset($params['type']) && $params['type'] === 'checkbox') {
            $tmpSet[$fname] = (!empty($value)) ? 1 : 0;
        // Для остальных типов сохраняем значение.
        } else {
            $tmpSet[$fname] = $value;
        }

        // Если указана специальная функция, которую следует выполнить при сохранении этой настройки
        if (!empty($params['onsave'])) {
            // Если это multiply, то это значит, что нужно умножить на указанное число введенное значение.
            if (!empty($params['onsave']['multiply'])) {
                $tmpSet[$fname] = round($tmpSet[$fname] * $params['onsave']['multiply']);
            }
            // Если это полноценная функция
            if (!empty($params['onsave']['func']) && is_callable($params['onsave']['func'])) {
                // Если функция нужна для обработки файла
                if ($params['type'] == 'file' && (isset($_POST[$fname]) || isset($_FILES[$fname]))) {
                    $tmpSet[$fname] = call_user_func($params['onsave']['func'], $tmpSet, $fname);
                    if (empty($tmpSet[$fname]))
                        unset($tmpSet[$fname]);
                    continue;
                // И если для обработки любых других данных
                } else {
                    $tmpSet[$fname] = call_user_func($params['onsave']['func'], $tmpSet, $fname);
                }
            }
        }

    }

    // Если настройки этого модуля нужно обьединять под один ключ
    if (in_array($module, $sysMods) && !in_array($module, $noSub)) {
        $_tmpSet = $config;
        $_tmpSet[$module] = $tmpSet;
        $tmpSet = $_tmpSet;
    }


    //save settings
    if (in_array($module, $sysMods))
        \Config::write($tmpSet);
    else
        \Config::write($tmpSet, $module);

    $_SESSION['message'][] = __('Saved');
    //clean cache
    $Cache = new \Cache('pages');
    $Cache->clean(CACHE_MATCHING_ANY_TAG, array('module_' . $module));
    redirect('/admin/settings.php?m=' . $module);
}





// Build form for settings editor
$_config = (in_array($module, $sysMods) && !in_array($module, $noSub)) ? $config[$module] : $config;

$output = '';
if (isset($settingsInfo) && count($settingsInfo)) {
    foreach ($settingsInfo as $fname => $params) {
        if (is_string($params)) {
            $output .= '<div class="col b30tm s12"><h5 class="light">' . h($params) . '</h5></div>';
            continue;
        }

        // Заполняем незаполненные значения значениями по умолчанию
        if (array_key_exists('type',$params) && $params['type'] == 'checkbox')
            $params = array_merge(array(
                'value' => '1',
                'checked' => '1',
            ), $params);

        $params = array_merge(array(
            'type' => 'text',
            'title' => '',
            'description' => '',
            'help' => '',
            'options' => array(),
            'attr' => array(),
            'grid-width' => 's12'
        ), $params);



        $currValue = (!empty($_config[$fname])) ? $_config[$fname] : false;
        if (!empty($params['onview'])) {
            if (!empty($params['onview']['division'])) {
                $currValue = round($currValue / $params['onview']['division']);
            }
        }


        $attr = '';
        if (!empty($params['attr']) && count($params['attr'])) {
            foreach ($params['attr'] as $attrk => $attrv) {
                $attr .= ' ' . h($attrk) . '="' . h($attrv) . '"';
            }
        }

        $output_ = '';

        // If we have function by create sufix after input field
        if (!empty($params['input_prefix_func']) && is_callable($params['input_prefix_func'])) {
            $output_ .= call_user_func($params['input_prefix_func'], $config, $fname);
        }
        if (!empty($params['input_prefix'])) {
            $output_ .= $params['input_prefix'];
        }

        switch ($params['type']) {
            case 'text':
            case 'number':
                $output_ .= '<div title="' . h($params['description']) . '" class="input-field col b10tm '.$params['grid-width'].'"><input type="'.$params['type'].'" id="' . h($fname) . '" name="' . h($fname) . '" value="' . $currValue . '"' . $attr . '  class="validate"/>';
                $output_ .= '<label for="' . h($fname) . '">' . $params["title"] . '</label>';
                // Help note
                if (!empty($params['help'])) $output_ .= '<small class="right">' . h($params['help']) . '</small>';
                $output_ .= '</div>';
                break;

            case 'checkbox':

                $id = md5(rand(0, 99999) + rand(0, 99999));

                $state = (!empty($params['checked']) && $currValue == $params['checked']) ? ' checked="checked"' : '';


                if (!empty($params['fields'])) {
                    if (false !== strpos($fname, 'sub_'))
                        $fname = mb_substr($fname, mb_strlen('sub_'));
                    $subParams = (!empty($_config[$params['fields']])) ? $_config[$params['fields']] : array();
                    if (count($subParams) && in_array($fname, $subParams))
                        $state = ' checked="checked"';

                    $fname = $params['fields']. '[' . $fname . ']';
                }


                $output_ .= '<p title="' . h($params['description']) . '" class="col '.$params['grid-width'].'"><input id="' . $id . '" type="checkbox" name="' . h($fname)
                . '" value="' . $params['value'] . '" ' . $state . '' . $attr . ' /><label for="' . $id . '">'.$params["title"].'</label></p>';
                break;

            case 'select':
                $options = '';
                if (count($params['options'])) {
                    foreach ($params['options'] as $value => $visName) {
                        $options_ = '';
                        $state = ($_config[$fname] == $value) ? ' selected="selected"' : '';

                        $attr_option = '';
                        if (!empty($params['options_attr'])) {
                            foreach ($params['options_attr'] as $k => $v) {
                                $attr_option .= ' ' . $k . '="' . $v . '"';
                            }
                        }

                        $options_ .= '<option ' . $state . $attr_option . ' value="'
                        . h($value) . '">' . h($visName) . '</option>';
                        $options .= sprintf($options_, getImgPath($value));
                    }
                }

                $output_ .= '<div title="' . h($params['description']) . '" class="input-field col b30tm '.$params['grid-width'].'"><select ' . $attr . ' name="' . h($fname) . '">' . $options . '</select>';
                $output_ .= '<label>'.$params["title"].'</label></div>';
                break;

            case 'file':
                $output_ .= '<div title="' . h($params['description']) . '" class="file-field input-field col '.$params['grid-width'].'">'
                                .'<input class="file-path validate" type="text"/>'
                                .'<div class="btn">'
                                  .'<span>File</span>'
                                  .'<input type="file" name="' . h($fname) . '"' . $attr . ' />'
                                .'</div>'
                            .'</div>';
                break;
        }


        // $params['description']
        $output .= ''
                   . $output_;



        // If we have function by create sufix after input field
        if (!empty($params['input_suffix_func']) && is_callable($params['input_suffix_func'])) {
            $output .= call_user_func($params['input_suffix_func'], $config, $fname);
        }
        if (!empty($params['input_suffix'])) {
            $output .= $params['input_suffix'];
        }

        $output .= '';
    }
    
}


include_once R.'admin/template/header.php';
?>

<form method="POST" action="settings.php?m=<?php echo $module; ?>" enctype="multipart/form-data">
    <div class="section">
        <div class="col s12">
                <h4 class="light"><?php echo $pageTitle; ?></h4>
                <?php echo $output; ?>
                
                <div class="section">
                    <button class="btn waves-effect waves-light b30tm" type="submit" name="send">
                        <?php echo __('Save'); ?>
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
        </div>
    </div>
</form>
<script>
    $(document).ready(function() {
        $('select').material_select();
    });
</script>


<?php include_once R.'admin/template/footer.php'; ?>
