<?php
/**
* @project    Atom-M CMS
* @package    Template redactor
* @url        https://atom-m.net
*/

include_once '../sys/boot.php';
include_once R.'admin/inc/adm_boot.php';


$pageTitle = __('Design - templates');
$pageNav = $pageTitle;
$pageNavr = '<a href="set_default_dis.php" onClick="return confirm(\'' . __('Return to default template, confirm text') . '\')">' . __('Return to default template') . '</a>&nbsp;|&nbsp;<a href="backup_dis.php" onClick="return confirm(\'' . __('Return to default template, confirm text') . '\')">' . __('Save current state of template') . '</a>';


$allowedFiles = array(
    'default' => array(
        'main.html.twig'             => __('Layout'),
        'addcommentform.html.twig'   => __('Add comment form'),
        'editcommentform.html.twig'  => __('Edit comment form'),
        'viewcomment.html.twig'      => __('View comment'),
        'captcha.html.twig'          => __('Captcha'),
        'infomessage.html.twig'      => __('Infomessage'),
        'infomessagegrand.html.twig' => __('Infomessagegrand'),
        'list.html.twig'             => __('List of materials'),
    ),
    'includes' => array(
        'header.html.twig'               => __('Site header'),
        'footer.html.twig'               => __('Site footer'),
        'moder_panel.html.twig'          => __('Entry moderation panel'),
        'moder_panel_comment.html.twig'  => __('Comment moderation panel'),
    ),
);


$Register = Register::getInstance();
$template_name = \Config::read('template');


/* ADD TEMPLATE */

if (!empty($_GET['ac']) && $_GET['ac'] === 'add_template') {
    // Модуль.
    $module = preg_replace('#[^a-z0-9_\-]#', '', (!empty($_POST['module'])) ? $_POST['module'] : '');
    $_GET['m'] = $module;
    // Название файла
	$filename = preg_replace('#[^a-z0-9_.\-]#', '', (!empty($_POST['title'])) ? $_POST['title'] : '');
    if (strpos($filename, '.') === false) $filename .= '.html.twig';
    $_GET['t'] = $filename;
    
	$code = (!empty($_POST['code'])) ? $_POST['code'] : '';
	
	if (empty($filename) || empty($code) || empty($module)) redirect('/admin/design.php?m=default&t=main.html.twig');
	
	// Путь до папки создаваемого шаблона
	$path = R.'template/' . $template_name . '/html/'.$module.'/';
    // Путь до создаваемого шаблона
	$path2 = R.'template/' . $template_name . '/html/'.$module.'/' . $filename;
	// Если такой файл уже существует
	if (file_exists($path2))
		$_SESSION['message'][] = __('This template is existed');
	// Если не существует файл, то создаем его и папку для него, если её нет.
    else {
        if (!file_exists($path))
            mkdir($path, 0755);
	
		file_put_contents($path2, $code);
		$_SESSION['message'][] = __('Template is created');
	}
}



// Получение данных

// path formating "TEMPLATE_ROOT/$type/$module/$filename"
// папка нахождения шаблона (1)
$module = '';
if (empty($_GET['d']) || !is_string($_GET['d'])) $_GET['d'] = 'html';
$type = trim($_GET['d']);
if ($type == 'html') {
    // папка нахождения шаблона (2)
    if (empty($_GET['m']) || !is_string($_GET['m'])) $_GET['m'] = 'default';
    $module = trim($_GET['m']);
    $Register['module'] = $module;
}
// название файла в этой папке
if (empty($_GET['t']) || !is_string($_GET['t'])) $_GET['t'] = 'main.html.twig';
$filename = trim($_GET['t']);



/* LIST OF TEMPLATES */
clearstatcache();

$custom_tpl = array();
// Получаем пути до папок с шаблонами
$dirs_tmp = glob(R.'template/' . $template_name . '/html/*', GLOB_ONLYDIR);
if (!empty($dirs_tmp)) {
    // Обходим каждую папку
    foreach ($dirs_tmp as $dir) {
        $mod = basename($dir);
        // Получаем список необходимых для работы модуля шаблонов
        $need_parts = \ModuleManager::getTemplateParts($mod);
        if (!empty($need_parts)) {
            $allowedFiles[$mod] = $need_parts;
        }
        
        // Проходим по каждому файлу в папке
        if ($mod_templates = opendir(R.'template/' . $template_name . '/html/'.$mod.'/')) {
            while (false !== ($tmp_name = readdir($mod_templates))) {
                // Если этот файл не указан в обязательных шаблонах модуля, если это папка модуля. или это файлы не из папки модуля
                if (strpos($tmp_name,'.') !== 0 && strpos($tmp_name,'.stand') === false
                    && (!isset($allowedFiles[$mod]) || !array_key_exists($tmp_name, $allowedFiles[$mod]))) {
                    if (!isset($custom_tpl[$mod]) || !is_array($custom_tpl[$mod]))
                        $custom_tpl[$mod] = array();
                    $custom_tpl[$mod][] = $tmp_name;
                }
            }
            closedir($mod_templates);
        }
	}
}

/* CSS STYLES */
clearstatcache();
$styles = array();
if ($mod_templates = opendir(R.'template/' . $template_name . '/css/')) {
    while (false !== ($tmp_name = readdir($mod_templates))) {
        if (strpos($tmp_name,'.css') && strpos($tmp_name,'.stand') === false)
            $styles[] = $tmp_name;
    }
}

/* JS */
clearstatcache();
$scripts = array();
if ($mod_templates = opendir(R.'template/' . $template_name . '/js/')) {
    while (false !== ($tmp_name = readdir($mod_templates))) {
        if (strpos($tmp_name,'.js') && strpos($tmp_name,'.stand') === false)
            $scripts[] = $tmp_name;
    }
}

/* SAVE CODE */

if(isset($_POST['send']) && isset($_POST['templ'])) {

    $template_file = R.'/template/' . $template_name . '/' . $type .'/' . (($type == 'html') ? $module . '/' : '') . $filename;
    
    if (!file_exists($template_file . '.stand') && file_exists($template_file)) {
        copy($template_file, $template_file . '.stand');
    }
    $file = fopen($template_file, 'w+');
	
	if(fputs($file, $_POST['templ'])) {
		$_SESSION['message'][] = __('Template is saved');
	} else {
		$_SESSION['message'][] = __('Template is not saved');
	}
	fclose($file);
}



/* SHOW CODE */

$path = R.'template/' . $template_name . '/' . $type .'/' . (($type == 'html') ? $module . '/' : '') . $filename;
if (!file_exists($path)) {
    $path = ROOT .'/template/' . $template_name . '/html/default/' . $filename;
    if (!file_exists($path)) {
        $_SESSION['message'][] = __('Requested file is not found');
        redirect('/admin/design.php');
    }
}
$template = file_get_contents($path);


include_once R.'admin/template/header.php';
echo '<form action="' . $_SERVER['REQUEST_URI'] . '" method="POST">';

?>

<div class="row">
    <div class="col s3">
        <div class="menuHTML5 section">
            <!-- Шаблоны созданные пользователем или создателями шаблона -->
            <details <?php if ($type == 'html' && array_key_exists($module,$custom_tpl) && in_array($filename,$custom_tpl[$module])) echo 'open' ?>>
            <summary><?php echo __('Your templates') ?></summary>
            <?php foreach ($custom_tpl as $mod => $files):
                $title = ('default' == $mod) ? __('Default') : __($mod,false,$mod);
                
                if (!empty($title)):?>
                <ul class="collection with-header" <?php if ($type == 'html' && $module == $mod) echo 'open' ?>>
                    <li class="collection-header"><?php echo $title; ?></li>
                    <?php foreach ($files as $file => $title): ?>
                    <a class="collection-item <?php if($module == $mod && $title == $filename) echo 'active'; ?>" href="design.php?d=html&t=<?php echo $title; ?>&m=<?php echo $mod; ?>"><?php echo $title; ?></a>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            <?php endforeach; ?>
            </details>
            
            <!-- Стили шаблона -->
            <details <?php if ($type == 'css') echo 'open' ?>>
            <summary><?php echo __('Style(CSS)') ?></summary>
            <ul class="collection">
                <?php foreach ($styles as $f_name): ?>
                    <a class="collection-item <?php if($f_name == $filename) echo 'active'; ?>" href="design.php?d=css&t=<?php echo $f_name; ?>"><?php echo $f_name; ?></a>
                <?php endforeach; ?>
            </ul>
            </details>
            <!-- javascript шаблона -->
            <details <?php if ($type == 'js') echo 'open' ?>>
            <summary><?php echo __('Scripts(JS)') ?></summary>
            <ul class="collection">
                <?php foreach ($scripts as $f_name): ?>
                    <a class="collection-item <?php if($f_name == $filename) echo 'active'; ?>" href="design.php?d=js&t=<?php echo $f_name; ?>"><?php echo $f_name; ?></a>
                <?php endforeach; ?>
            </ul>
            </details>
            
            <!-- Необходимые для работы модуля шаблоны -->
            <details <?php if ($type == 'html' && array_key_exists($module,$allowedFiles) && array_key_exists($filename,$allowedFiles[$module])) echo 'open' ?>>
            <summary><?php echo __('Template') ?></summary>
            <?php foreach ($allowedFiles as $mod => $files):
                $title = ('default' == $mod) ? __('Default') : (('includes' == $mod) ? __('Includes') : __($mod,false,$mod));
                
                if (!empty($title)):?>
                <ul class="collection with-header" <?php if ($type == 'html' && $module == $mod) echo 'open' ?>>
                    <li class="collection-header"><?php echo $title; ?></li>
                    <?php foreach ($files as $file => $title): ?>
                    <a class="collection-item <?php if($module == $mod && $file == $filename) echo 'active'; ?>" href="design.php?d=html&t=<?php echo $file; ?>&m=<?php echo $mod; ?>" title="<?php echo $file; ?>"><?php echo $title; ?></a>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            <?php endforeach; ?>
            </details>
        </div>
    </div>
        

    <!-- EDIT FORM -->
    <div class="col s12 m9">
        <a class="btn right modal-trigger" href="#addform"><i class="mdi-action-backup"></i> <?php echo __('Add template') ?></a>
        <h4 class="light"><?php echo __('Template editor').' - '.$filename; ?></h4>

        <div class="row b10tm">
            <textarea title="<?php echo __('Template code') ?>" wrap="off" id="tmpl" name="templ"><?php print h($template); ?></textarea>
            <div class="center b10tm">
                <input class="btn" type="submit" name="send" value="<?php echo __('Save') ?>" />
            </div>
        </div>
    </div>

</div>
</form>


<!-- ADD FORM -->

<div id="addform" class="modal modal-fixed-footer">
    <form action="design.php?ac=add_template" method="POST">
    <div class="modal-content">
        <h4><?php echo __('Adding template') ?></h4>
        <p>
            <div class="input-field col s2">
                <input id="module_name" type="text" name="module" placeholder="<?php echo __('Module path: info') ?>" value="<?php echo $module ?>" required/>
                <label for="module_name"><?php echo __('Module') ?></label>
            </div>
            <div class="input-field col s10">
                <input id="file_title" type="text" name="title" placeholder="<?php echo __('File name: info') ?>" required/>
                <label for="file_title"><?php echo __('Name') ?></label>
            </div>
            <div class="input-field col s12">
                <?php echo __('Template code') ?>
                <textarea name="code" wrap="off" id="new_tmpl"></textarea>
            </div>
        </p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat">ОТМЕНИТЬ</a>
        <input type="submit" value="<?php echo __('Add') ?>" name="send" class="btn" />
    </div>
    </form>
</div>



<script type="text/javascript" src="js/codemirror/codemirror.js"></script>
<script type="text/javascript" src="js/codemirror/mode/javascript/javascript.js"></script>
<script type="text/javascript" src="js/codemirror/mode/xml/xml.js"></script>
<script type="text/javascript" src="js/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script type="text/javascript" src="js/codemirror/mode/css/css.js"></script>

<link rel="StyleSheet" type="text/css" href="js/codemirror/codemirror.css" />
<script type="text/javascript">
$(document).ready(function(){
    // Редактирование
    var editor = CodeMirror.fromTextArea(document.getElementById("tmpl"), {
        lineNumbers: true,
        matchBrackets: true,
        indentUnit: 4,
        mode: "text/<?php echo ($type == 'js') ? 'javascript' : $type; ?>"
    });
    editor.setSize('100%', '100%');
    
    // Добавление
    var editor2 = CodeMirror.fromTextArea(document.getElementById("new_tmpl"), {
        lineNumbers: true,
        matchBrackets: true,
        indentUnit: 4,
        mode: "text/html"
    });
    editor2.setSize('100%', '100%');
    
    
    // For modals
    $('.modal-trigger').leanModal();
});
</script>


<div class="row">
    <div class="col s12">
        <h5 class="light"><?php echo __('Global markers') ?></h5>
        <ul>
            <li><b>{{ content }}</b> - <?php echo __('Base content page') ?></li>
            <li><b>{{ title }}</b> - <?php echo __('Title for page') ?></li>
            <li><b>{{ description }}</b> - <?php echo __('Meta-Description') ?></li>
            <li><b>{{ fps_wday }}</b> - <?php echo __('Day mini') ?></li>
            <li><b>{{ fps_date }}</b> - <?php echo __('Date') ?></li>
            <li><b>{{ fps_time }}</b> - <?php echo __('Time') ?></li>
            <li><b>{{ atm_user.name }}</b> - <?php echo __('Nick of current user') ?></li>
            <li><b>{{ atm_user.group }}</b> - <?php echo __('Group of current user') ?></li>
            <li><b>{{ categories }}</b> - <?php echo __('categories list in section') ?></li>
            <li><b>{{ counter }}</b> - <?php echo __('counter') ?></li>
            <li><b>{{ fps_year }}</b> - <?php echo __('Year') ?></li>
            <li><b>{{ powered_by }}</b> - <?php echo __('About powered by') ?></li>
            <li><b>{{ comments }}</b> - <?php echo __('About comments marker') ?></li>
            <li><b>{{ personal_page_link }}</b> - <?php echo __('About personal_page_link marker') ?></li>
        </ul>
    </div>
</div>

<?php include_once 'template/footer.php'; ?>

