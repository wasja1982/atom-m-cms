<?php


$path = dirname(__FILE__).DIRECTORY_SEPARATOR.'sys/settings/config.php';
$set = include $path;

if (isset($set)) {

    if (isset($set['db'])) {
        $set['__db__'] = $set['db'];
    }


    $set['system_modules'] = array (
        0 => '__sys__',
        1 => '__seo__',
        2 => '__secure__',
        3 => '__rss__',
        4 => '__sitemap__',
        5 => '__preview__',
        6 => '__watermark__',
        7 => '__db__',
        8 => '__home__',
    );
    $set['__css__'] = array ();
    $set['__js__'] = array (
        '/data/js/jquery.js',
        '/data/js/atom.js',
    );

    if ($fopen=@fopen($path, 'w')) {
        $data = '<?php ' . "\n" . 'return ' . var_export($set, true) . "\n" . '?>';
        fputs($fopen, $data);
        fclose($fopen);
    }

}


include_once 'sys/boot.php';


$DB = getDB();


// convert section tables
$DB->query("ALTER TABLE `" . $DB->getFullTableName("news") . "` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("stat") . "` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("loads") . "` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("foto") . "` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");

$DB->query("ALTER TABLE `" . $DB->getFullTableName("comments") . "` ADD `parent_id` INT(11) NULL DEFAULT '0' AFTER `id`;");

$DB->query("ALTER TABLE `" . $DB->getFullTableName("posts") . "` ADD `id_forum` INT(11) NULL DEFAULT '0' AFTER `id_theme`;");
$DB->query("UPDATE `" . $DB->getFullTableName("posts") . "` SET `id_forum` = (SELECT `id_forum` FROM `" . $DB->getFullTableName("themes") . "` WHERE `id` = `" . $DB->getFullTableName("posts") . "`.`id_theme`);");

$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("news_add_fields") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("loads_add_fields") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("stat_add_fields") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("users_add_fields") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("news_add_content") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("loads_add_content") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("stat_add_content") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("users_add_content") . "`;");

$DB->query("CREATE TABLE IF NOT EXISTS `" . $DB->getFullTableName("add_fields") . "` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `field_id` INT(11) NOT NULL,
    `module` VARCHAR(100) NOT NULL,
    `type` VARCHAR(10) NOT NULL,
    `name` VARCHAR(100) NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `size` INT(11) NOT NULL,
    `params` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;");

$DB->query("RENAME TABLE IF EXISTS `" . $DB->getFullTableName("news_sections") . "` TO `" . $DB->getFullTableName("news_categories") . "`");
$DB->query("RENAME TABLE IF EXISTS `" . $DB->getFullTableName("foto_sections") . "` TO `" . $DB->getFullTableName("foto_categories") . "`");
$DB->query("RENAME TABLE IF EXISTS `" . $DB->getFullTableName("loads_sections") . "` TO `" . $DB->getFullTableName("loads_categories") . "`");
$DB->query("RENAME TABLE IF EXISTS `" . $DB->getFullTableName("stat_sections") . "` TO `" . $DB->getFullTableName("stat_categories") . "`");

$DB->query("ALTER TABLE `" . $DB->getFullTableName("posts") . "` DROP `attaches`;");

$DB->query("CREATE TABLE IF NOT EXISTS `" . $DB->getFullTableName("attaches") . "` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `entity_id` INT NOT NULL,
    `user_id` INT NOT NULL ,
    `attach_number` INT NOT NULL ,
    `filename` VARCHAR( 100 ) NOT NULL ,
    `size` BIGINT NOT NULL ,
    `date` DATETIME NOT NULL ,
    `is_image` ENUM( '0', '1' ) DEFAULT '0' NOT NULL ,
    `module` varchar(10) default 'news' NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;");

$DB->query("INSERT INTO `" . $DB->getFullTableName("attaches") . "` 
SELECT '0' as id, post_id as entity_id, user_id, attach_number, filename, size, date, is_image, 'forum' AS module FROM `" . $DB->getFullTableName("forum_attaches") . "`
UNION
SELECT '0' as id, entity_id, user_id, attach_number, filename, size, date, is_image, 'loads' AS module FROM `" . $DB->getFullTableName("loads_attaches") . "`
UNION
SELECT '0' as id, entity_id, user_id, attach_number, filename, size, date, is_image, 'news' AS module FROM `" . $DB->getFullTableName("news_attaches") . "`
UNION
SELECT '0' as id, entity_id, user_id, attach_number, filename, size, date, is_image, 'stat' AS module FROM `" . $DB->getFullTableName("stat_attaches") . "`
ORDER BY date ASC;");

$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("forum_attaches") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("loads_attaches") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("news_attaches") . "`;");
$DB->query("DROP TABLE IF EXISTS `" . $DB->getFullTableName("stat_attaches") . "`;");

die(__('Operation is successful'));