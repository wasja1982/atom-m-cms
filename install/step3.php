<?php
include_once '../sys/boot.php';

$installed_modules = \ModuleManager::getInstalledModules();
$new_modules = \ModuleManager::getNewModules();
$modules = array_merge($installed_modules, $new_modules);

if (isset($_POST['send'])) {
    $need_modules = isset($_POST['modules']) && is_array($_POST['modules']) ? $_POST['modules'] : array();

    foreach ($installed_modules as $module) {
        if (!in_array($module, array('home', 'users')) && !isset($need_modules[$module])) {
            \ModuleManager::uninstallModule($module);
        }
    }
    foreach ($new_modules as $module) {
        if (!in_array($module, array('home', 'users')) && isset($need_modules[$module])) {
            \ModuleManager::installModule($module);
        }
    }
    header('Location: /');
    die();
}
?><!doctype html>
<html>
    <head>
        <title>Atom-M CMS - вместе в будущее</title>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link type="text/css" rel="StyleSheet" href="css/style.css" />
        <script language="JavaScript" type="text/javascript" src="../data/js/jquery.js"></script>
    </head>
    <body>

        <div id="container">
            <div id="descr">
                <div id="newv"></div>

                <h3>Установить модули</h3>

                <form method="post" action="">
                    <ul>
                        <?php foreach ($modules as $module) : ?>
                            <li>
                                <input type="checkbox" id="cb_<?php echo $module; ?>" name="modules[<?php echo $module; ?>]"<?php if (in_array($module, $installed_modules)) echo ' checked'; ?><?php if (in_array($module, array('home', 'users'))) echo ' disabled="disabled"' ?>>
                                <label for="cb_<?php echo $module; ?>"><?php echo __($module, false, $module); ?></label>
                            </li>
                        <?php endforeach; ?>
                    </ul>
            </div>
            <input class="btn" type="submit" name="send" value="ЗАВЕРШИТЬ" />
        </form> 
        <br />


    </div>

    <div id="footer">
        <div style="float:left;">
            <a href="https://atom-m.net/">Официальный сайт</a>
            <a href="https://dev.atom-m.net/forum/">Форум</a>
            <a href="https://bitbucket.org/atom-m/cms/wiki/Home">WIKI</a>
        </div>
        <div style="clear:both;"></div>
    </div>

</body>
</html>